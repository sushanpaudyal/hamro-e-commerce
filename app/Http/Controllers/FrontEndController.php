<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\ProductAttribute;
use Illuminate\Http\Request;

class FrontEndController extends Controller
{
    // Product Detail Page
    public function productDetail($slug){
        $product = Product::with(['images', 'attributes'])->where('slug', $slug)->first();
        $product_id = $product->id;
        $relatedProducts = Product::where('id', '!=', $product_id)->where(['category_id' => $product->category_id])->take(8)->get();
        $total_stock = ProductAttribute::where('product_id', $product->id)->sum('stock');
        return view ('front.product.productDetail', compact('product', 'relatedProducts', 'total_stock'));
    }

    // Category Page
    public function categoryPage($slug){
        // Show 404 Page if Category Does Not Exist
        $countCategory = Category::where(['slug' => $slug])->count();
        if($countCategory == 0){
            abort(404);
        }


        $categoryDetails = Category::where('slug', $slug)->first();
        $categories = Category::where('parent_id', $categoryDetails->id)->get();

        $productsAll = Product::where('category_id', $categoryDetails->id)->get();
        return view ('front.product.category', compact('categoryDetails', 'categories', 'productsAll'));
    }

    // Featured Products
    public function featuredProducts(){
        $title = "Featured Products";
        $featuredProducts = Product::where('featured_product', 1)->latest()->get();
        return view ('front.product.featured', compact('title', 'featuredProducts'));
    }

    // Get Product Price
    public function getProductPrice(Request $request){
        $data = $request->all();
        $proArr = explode("-", $data['idSize']);
       $proAttr = ProductAttribute::where(['product_id' => $proArr[0], 'size' => $proArr[1]])->first();
       echo $proAttr->price;
       echo "#";
       echo $proAttr->stock;
    }
}
