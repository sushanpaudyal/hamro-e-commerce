<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use DB;

class CategoryController extends Controller
{
    // Category Index
     public function category(){
         Session::put('admin_page', 'category');
         $categories = Category::latest()->get();
         return view ('admin.category.index', compact('categories'));
     }

     // Add Category
    public function addCategory(){
        Session::put('admin_page', 'category');
        $categories = Category::where('parent_id', 0)->orderBy('category_name', 'ASC')->get();
        return view ('admin.category.add', compact('categories'));
    }

    // Store Category
    public function storeCategory(Request $request){
         $data = $request->all();
        $validateData = $request->validate([
            'category_name' => 'required|max:255',
            'category_code' => 'required|min:6',
            'parent_id' => 'required'
        ]);
        $category = new Category();
        $category->category_name = ucwords(strtolower($data['category_name']));
        $category->category_code = $data['category_code'];
        $category->slug = Str::slug($data['category_name']);
        $category->parent_id = $data['parent_id'];
        if(empty($data['description'])){
            $category->description = "";
        } else {
            $category->description = $data['description'];
        }
        if (empty($data['status'])){
            $category->status = 0;
        } else {
            $category->status = 1;
        }

        $random = Str::random(10);
        if($request->hasFile('image')){
            $image_tmp = $request->file('image');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = $random . '.' . $extension;
                $image_path = 'public/uploads/category/' . $filename;
                Image::make($image_tmp)->save($image_path);
                $category->image = $filename;
            }
        }

        $random = Str::random(10);
        if($request->hasFile('ad_1')){
            $image_tmp = $request->file('ad_1');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = $random . '.' . $extension;
                $image_path = 'public/uploads/category/' . $filename;
                Image::make($image_tmp)->save($image_path);
                $category->ad_1 = $filename;
            }
        }

        $random = Str::random(10);
        if($request->hasFile('ad_2')){
            $image_tmp = $request->file('ad_2');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = $random . '.' . $extension;
                $image_path = 'public/uploads/category/' . $filename;
                Image::make($image_tmp)->save($image_path);
                $category->ad_2 = $filename;
            }
        }

        $category->save();
        Session::flash('success_message', 'Category Has Been Added Successfully');
        return redirect()->back();
    }


    // Edit Category
    public function editCategory($id){
        Session::put('admin_page', 'category');
        $myCategory = Category::findOrFail($id);
        $categories = Category::where('parent_id', 0)->orderBy('category_name', 'ASC')->get();
        return view ('admin.category.edit', compact('categories', 'myCategory'));
    }


    // Update Category
    public function updateCategory(Request $request, $id){
        $data = $request->all();
        $validateData = $request->validate([
            'category_name' => 'required|max:255',
            'category_code' => 'required|min:6',
        ]);
        $category = Category::findOrFail($id);
        $category->category_name = ucwords(strtolower($data['category_name']));
        $category->category_code = $data['category_code'];
        $category->slug = Str::slug($data['category_name']);
        $category->parent_id = $data['parent_id'];
        if(empty($data['description'])){
            $category->description = "";
        } else {
            $category->description = $data['description'];
        }
        if (empty($data['status'])){
            $category->status = 0;
        } else {
            $category->status = 1;
        }

        $random = Str::random(10);
        if($request->hasFile('image')){
            $image_tmp = $request->file('image');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = $random . '.' . $extension;
                $image_path = 'public/uploads/category/' . $filename;
                Image::make($image_tmp)->save($image_path);
                $category->image = $filename;
            }
        }

        $random = Str::random(10);
        if($request->hasFile('ad_1')){
            $image_tmp = $request->file('ad_1');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = $random . '.' . $extension;
                $image_path = 'public/uploads/category/' . $filename;
                Image::make($image_tmp)->save($image_path);
                $category->ad_1 = $filename;
            }
        }

        $random = Str::random(10);
        if($request->hasFile('ad_2')){
            $image_tmp = $request->file('ad_2');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = $random . '.' . $extension;
                $image_path = 'public/uploads/category/' . $filename;
                Image::make($image_tmp)->save($image_path);
                $category->ad_2 = $filename;
            }
        }


        $image_path = 'public/uploads/category/';
        if(!empty($data['image'])) {
            if (!empty($data['current_image'])){
                if (file_exists($image_path . $data['current_image'])) {
                    unlink($image_path . $data['current_image']);
                }
        }
        }

        $category->save();
        Session::flash('success_message', 'Category Has Been Updated Successfully');
        return redirect()->back();
    }

    // Delete Category
    public function deleteCategory($id){
         $category = Category::findOrFail($id);
         $category->delete();
         DB::table('categories')->where('parent_id', $id)->delete();
         DB::table('products')->where('category_id', $id)->delete();

         $image_path = 'public/uploads/category/';
         if(!empty($category->image)){
             if(file_exists($image_path.$category->image)){
                 unlink($image_path.$category->image);
             }
         }
        Session::flash('success_message', 'Category Has Been deleted Successfully');
        return redirect()->back();
    }

    public function updateCategoryStatus(Request  $request){
         if($request->ajax()){
             $data = $request->all();
             if($data['status'] == 'Active'){
                 $status = 0;
             } else {
                 $status = 1;
             }
             Category::where('id', $data['category_id'])->update(['status' => $status]);
             return response()->json(['status' => $status, 'category_id' => $data['category_id']]);
         }
    }
}
