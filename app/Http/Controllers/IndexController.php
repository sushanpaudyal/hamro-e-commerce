<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    // Index Page
    public function index(){
        $featuredProducts = Product::where('featured_product', 1)->where('status', 1)->latest()->get();
        $mainCategories = Category::where('parent_id', 0)->where('status', 1)->orderby('category_name', 'ASC')->get();
        $frontCategories = Category::where('parent_id', 0)->where('status', 1)->orderby('category_name', 'ASC')->take(6)->get();
        return view ('front.index', compact('featuredProducts','mainCategories', 'frontCategories'));
    }
}
