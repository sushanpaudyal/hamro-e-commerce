<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class FrontUserController extends Controller
{
    // User Login
    public function userLogin(){
        return view('front.userLogin');
    }

    public function loginUser(Request $request){
        $data = $request->all();
        if(Auth::attempt(['email' => $data['email'], 'password' => $data['password'], 'verified' => 1])){
            Session::put('frontSession', $data['email']);
            return redirect('/cart');
        } else {
            return redirect()->back()->with('flash_message_error', 'Your Credentials does not match with our database');
        }
    }

    // Logout
    public function frontUserLogout(){
        Auth::logout();
        Session::forget('frontSession');
        return redirect('/user/login');
    }

    // Register User
    public function userRegister(Request $request){
        $data = $request->all();
        $userCount = User::where('email', $data['email'])->count();
        if($userCount > 0){
            return redirect()->back()->with('flash_message_error2', 'Email Already Exist in the Database');

        } else {
            $user = new User();
            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->password = bcrypt($data['password']);
            $user->save();

            $email = $data['email'];
            $messageData = [
               'email' => strtolower($data['email']),
               'name' => ucwords(strtolower($data['name'])),
               'code' => base64_encode($data['email'])
            ];
            Mail::send('email.verify', $messageData, function ($message) use ($email){
                $message->to($email)->subject('E-Mail Verification Message');
            });

            return redirect()->back()->with('flash_message_success', 'An email verification link has been sent to your email. Please Verify Your Email');
        }
    }

    public function confirmAccount($email){
        $email = base64_decode($email);
        $userCount = User::where('email', $email)->count();
        if($userCount > 0){
            $userDetails = User::where('email', $email)->first();
            if($userDetails->verified == 1){
                return redirect('/user/login')->with('flash_message_success', 'Your Email is already activated');
            } else {
                User::where('email', $email)->update(['verified' => 1]);
                $messageData = [
                    'email' => $email,
                    'name' => $userDetails->name
                ];
                Mail::send('email.welcome', $messageData, function ($message) use ($email){
                    $message->to($email)->subject('Welcome To Hamroshop ! Happy Shopping');
                });
                return redirect('/user/login')->with('flash_message_success', 'Your Email Has Been Activated');

            }
        } else {
            abort(404);
        }
    }
}
