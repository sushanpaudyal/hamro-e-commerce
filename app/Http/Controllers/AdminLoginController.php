<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class AdminLoginController extends Controller
{
    // Admin Login
    public function adminLogin(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            if(Auth::guard('admin')->attempt(['email' => $data['email'], 'password' => $data['password']])){
                return redirect('/admin/dashboard');
            } else {
                return  redirect()->back();
            }
        }
       return view ('admin.auth.login');
    }

    // Admin Dashboard
    public function dashboard(){
        Session::put('admin_page', 'dashboard');
        return view ('admin.dashboard');
    }

    // Admin Logout
    public function adminLogout(){
        Auth::guard('admin')->logout();
        return redirect('/admin/login');
    }

    // Forget Password
    public function forgetPassword(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            $validateData = $request->validate([
               'email' => 'required'
            ]);
            $adminCount = Admin::where('email', $data['email'])->count();
            if($adminCount == 0){
                return redirect()->back()->with('error_message', 'User Doesnot Exist in Our Database');
            }
            // Get Admin Details
            $adminDetails = Admin::where('email', $data['email'])->first();
            // Generate Password
            $random_password = Str::random(10);
            // Encode Password
            $new_password = bcrypt($random_password);
            // Update Password
            Admin::where('email', $data['email'])->update(['password' => $new_password]);
            // Send Email
            $email = $data['email'];
            $name = $adminDetails->name;
            $messageData = ['email' => $data['email'], 'password' => $random_password, 'name' => $name];
            Mail::send('email.forgetpassword', $messageData, function($message) use ($email){
                $message->to($email)->subject('New Password - Hamro Shop Ecommerce');
            });

            return redirect()->back()->with('success_message', 'Please Check your email for updated password');

        }
        return view ('admin.auth.forget');
    }
}
