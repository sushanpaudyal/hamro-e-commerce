<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Coupon;
use App\Models\Product;
use App\Models\ProductAttribute;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class FrontProductController extends Controller
{
    // Add to Cart
    public function addToCart(Request $request){

        Session::forget('CouponAmount');
        Session::forget('CouponCode');

        if($request->isMethod('post')){
            $data = $request->all();
            if (empty($data['user_email'])){
                $data['user_email'] = "";
            }

            $session_id = Session::get('session_id');
            if(empty($session_id)){
                $session_id = Str::random(40);
                Session::put('session_id', $session_id);
            }
            $sizeArr = explode("-", $data['size']);


            $countProducts = DB::table('carts')->where(['product_id' => $data['product_id'], 'size' => $sizeArr[1], 'session_id' => $session_id])->count();
            if($countProducts > 0){
                return redirect()->back()->with('flash_message_error', 'Product already exist in cart');
            } else {
                $cart = new Cart();
                $cart->product_id = $data['product_id'];
                $cart->product_name = $data['product_name'];
                $cart->price = $data['price'];
                $cart->size = $sizeArr[1];
                $cart->quantity = $data['quantity'];
                $cart->user_email = $data['user_email'];
                $cart->session_id = $session_id;
                $cart->save();
            }


            return redirect()->route('cart');
        }
    }

    // View Cart
    public function cart(){
        $session_id = Session::get('session_id');
        $userCart = DB::table('carts')->where(['session_id' => $session_id])->get();

        foreach($userCart as $key => $product){
            $productDetails = Product::where('id', $product->product_id)->first();
            $userCart[$key]->image = $productDetails->image;
        }

        return view ('front.product.cart', compact('userCart'));
    }

    // Delete Cart Items
    public function deleteCart($id = null){
        DB::table('carts')->where('id', $id)->delete();
        return redirect()->back()->with('flash_message_info', 'Cart Item Deleted Successfully');
    }

    // Update Cart Quantity
    public function updateCartQuantity($id = null, $quantity = null){

        Session::forget('CouponAmount');
        Session::forget('CouponCode');

        $getCartDetails = DB::table('carts')->where('id', $id)->first();

        $getAttributeStock = ProductAttribute::where('size', $getCartDetails->size)->first();
        $updated_quantity = $getCartDetails->quantity + $quantity;

        if($getAttributeStock->stock >= $updated_quantity){
            DB::table('carts')->where('id', $id)->increment('quantity', $quantity);
            return redirect()->back()->with('flash_message_info', 'Product Has Been Updated in the Cart');
        } else {
            return redirect()->back()->with('flash_message_error', 'Product Required Quantity is out of stock');

        }


    }


    // Coupons
    public function applyCoupon(Request $request){
        $data = $request->all();
        $couponCount = Coupon::where('coupon_code', $data['coupon_code'])->count();
        if($couponCount == 0){
            return redirect()->back()->with('flash_message_error', 'Invalid Coupon Code');
        } else {
            $couponDetails = Coupon::where('coupon_code', $data['coupon_code'])->first();
            // If Coupon is InActive
            if($couponDetails->status == 0){
                return redirect()->back()->with('flash_message_error', 'Coupon is Not Active Right Now');
            }

            // If Coupon Is Expired
            $expiry_date = $couponDetails->expiry_date;
            $current_date = date('Y-m-d');
            if($expiry_date < $current_date){
                return redirect()->back()->with('flash_message_error', 'Coupon has been Expired');

            }

            $session_id = Session::get('session_id');
            // Getting Total Amount for Cart
            $userCart = DB::table('carts')->where(['session_id' => $session_id])->get();
            $total_amount = 0;
            foreach ($userCart as $item){
                $total_amount = $total_amount + ($item->price * $item->quantity);
            }

            if($couponDetails->amount_type == "Fixed"){
                $couponAmount = $couponDetails->amount;
            } else {
                $couponAmount = $total_amount * ($couponDetails->amount / 100);
            }

            Session::put('CouponAmount', $couponAmount);
            Session::put('CouponCode', $data['coupon_code']);
            return redirect()->back()->with('flash_message_error', 'Coupon has been applied');


        }
    }
}
