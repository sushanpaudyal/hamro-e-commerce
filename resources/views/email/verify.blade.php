<!doctype html>
<html>
<head>
    <title>E-Mail Verification Message</title>
</head>
<body>
     <h2 style="font-size: 1.6rem; font-weight: 400; color: #333">Hi {{ $name }}</h2>
     <p style="color: #777; line-height: 1.9">
         Please Click on the link below to complete the verification process for <span style="display: block; color: #000;"> {{ $email }}</span>
     </p>
     <a href="{{ url('/confirm/'.$code) }}" style="text-decoration: none; background: #eb5339; display: inline-block; padding: 10px 30px; color: #fff; border: 1px solid #eb5339;">
         Verify Your Email Address
     </a>
     <p>
         If you don't attempt to verify your email address with Hamro Shop. Please Delete this email.
     </p>
<p>
    Hamro Shop
</p>
</body>
</html>
