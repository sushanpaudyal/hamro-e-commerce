<head>
    <title>Etro Store - Premium Multipurpose HTML5/CSS3 Theme</title>
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- FAVICONS -->
    <link rel="shortcut icon" href="icons/favicon.png" />

    <!-- GOOGLE WEB FONTS -->
    <link rel="stylesheet" href="{{ asset('public/frontend/css/font-awesome.min.css') }}">

    <!-- BOOTSTRAP 3.3.7 CSS -->
    <link rel="stylesheet" href="{{ asset('public/frontend/css/bootstrap.min.css') }}" />

    <!-- SLICK v1.6.0 CSS -->
    <link rel="stylesheet" href="{{ asset('public/frontend/css/slick-1.6.0/slick.css') }}" />

    <link rel="stylesheet" href="{{ asset('public/frontend/css/jquery.fancybox.css') }}" />
    <link rel="stylesheet" href="{{ asset('public/frontend/css/yith-woocommerce-compare/colorbox.css') }}" />
    <link rel="stylesheet" href="{{ asset('public/frontend/css/owl-carousel/owl.carousel.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('public/frontend/css/owl-carousel/owl.theme.default.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('public/frontend/css/js_composer/js_composer.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('public/frontend/css/woocommerce/woocommerce.css') }}" />
    <link rel="stylesheet" href="{{ asset('public/frontend/css/yith-woocommerce-wishlist/style.css') }}" />

    <link rel="stylesheet" href="{{ asset('public/frontend/css/custom.css') }}" />
    <link rel="stylesheet" href="{{ asset('public/frontend/css/app-orange.css') }}" id="theme_color" />
    <link rel="stylesheet" href="" id="rtl" />
    <link rel="stylesheet" href="{{ asset('public/frontend/css/app-responsive.css') }}" />


    @yield('css')
</head>
