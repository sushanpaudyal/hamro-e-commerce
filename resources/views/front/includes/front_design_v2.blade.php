<!DOCTYPE html>
<html lang="en">

@include('front.includes.head')

<body class="product-template-default single single-product woocommerce woocommerce-page">
<div class="body-wrapper theme-clearfix">

    @include('front.includes.header')


  @yield('content')


@include('front.includes.footer')
