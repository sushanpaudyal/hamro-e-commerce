<!DOCTYPE html>
<html lang="en">

@include('front.includes.head')

<body class="page home-style1">
<div class="body-wrapper theme-clearfix">

    @include('front.includes.header')


  @yield('content')


@include('front.includes.footer')
