@extends('front.includes.front_design_v2')

@section('css')
    <link rel="stylesheet" href="{{ asset('public/frontend/passcheck/css/passtrength.css') }}">
@endsection





@section('content')
    <div class="listings-title">
        <div class="container">
            <div class="wrap-title">
                <h1>My Account</h1>
                <div class="bread">
                    <div class="breadcrumbs theme-clearfix">
                        <div class="container">
                            <ul class="breadcrumb">
                                <li>
                                    <a href="home_page_1.html">Home</a>
                                    <span class="go-page"></span>
                                </li>

                                <li class="active">
                                    <span>My account</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div id="contents" role="main" class="main-page col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="single page type-page status-publish hentry">
                    <div class="entry">
                        <div class="entry-content">
                            <header>
                                <h2 class="entry-title">My Account</h2>
                            </header>

                            <div class="entry-content">
                                <div class="woocommerce">
                                    <div class="col2-set row" id="customer_login">
                                        <div class="col-lg-6">
                                            <h2>Login</h2>
                                            @if(Session::has('flash_message_error'))
                                                <div class="alert alert-primary text-center" role="alert" style="color: red;">
                                                    {{ Session::get('flash_message_error') }}
                                                </div>
                                            @endif

                                            <form method="post" class="login" action="{{ route('loginUser') }}">
                                                @csrf
                                                <p class="form-row form-row-wide">
                                                    <label for="username">
                                                        Username or email address <span class="required">*</span>
                                                    </label>

                                                    <input type="text" class="input-text" name="email" id="email" />
                                                </p>

                                                <p class="form-row form-row-wide">
                                                    <label for="password">
                                                        Password <span class="required">*</span>
                                                    </label>

                                                    <input class="input-text" type="password" name="password" id="password" />
                                                </p>



                                                <p class="form-row">
                                                    <input type="submit" class="button"  value="Login" />
                                                </p>


                                            </form>

                                            <p class="lost_password">
                                                <a href="lost_password.html">Lost your password?</a>
                                            </p>
                                        </div>

                                        <div class="col-lg-6">
                                            <h2>Register</h2>

                                            @if(Session::has('flash_message_error2'))
                                                <div class="alert alert-primary text-center" role="alert" style="color: red;">
                                                    {{ Session::get('flash_message_error2') }}
                                                </div>
                                            @endif

                                            @if(Session::has('flash_message_success'))
                                                <div class="alert alert-primary text-center" role="alert" style="color: green;">
                                                    {{ Session::get('flash_message_success') }}
                                                </div>
                                            @endif

                                            <form method="post" class="register" action="{{ route('userRegister') }}">
                                                @csrf
                                                <p class="form-row form-row-wide">
                                                    <label for="name">
                                                        Name <span class="required">*</span>
                                                    </label>

                                                    <input type="text" class="input-text" name="name" id="name"  />
                                                </p>

                                                <p class="form-row form-row-wide">
                                                    <label for="reg_email">
                                                        Email address <span class="required">*</span>
                                                    </label>

                                                    <input type="email" class="input-text" name="email" id="reg_email"  />
                                                </p>

                                                <p class="form-row form-row-wide">
                                                    <label for="reg_password">
                                                        Password <span class="required">*</span>
                                                    </label>

                                                    <input type="password" class="input-text" name="password" id="reg_password" />
                                                </p>

                                                <p class="form-row">
                                                    <input type="submit" class="button disabled" value="Register" />
                                                </p>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('js')
    <script src="{{ asset('public/frontend/passcheck/js/jquery.passtrength.min.js') }}"></script>

    <script>
        $('#reg_password').passtrength({
            minChars: 6,
            passwordToggle:true,
            tooltip: true,
            eyeImg :"http://localhost/hamroshop/public/uploads/default/eye.svg"
        });
    </script>
@endsection
