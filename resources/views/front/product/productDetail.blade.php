@extends('front.includes.front_design_v2')

@section('content')

    <div class="listings-title">
        <div class="container">
            <div class="wrap-title">
                <h1>{{ $product->product_name }}</h1>
                <div class="bread">
                    <div class="breadcrumbs theme-clearfix">
                        <div class="container">
                            <ul class="breadcrumb">
                                <li><a href="{{ route('index') }}">Home</a><span class="go-page"></span></li>
                                <li><a href="group_product.html">{{ $product->category->category_name }}</a><span class="go-page"></span></li>
                                <li class="active"><span>{{ $product->product_name }}</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(Session::has('flash_message_error'))
    <div class="alert alert-primary text-center" role="alert" style="color: red;">
        {{ Session::get('flash_message_error') }}
    </div>
    @endif

    <div class="container">
        <div class="row">
            <div id="contents-detail" class="content col-lg-12 col-md-12 col-sm-12" role="main">
                <div id="container">
                    <div id="content" role="main">
                        <div class="single-product clearfix">
                            <div id="product-01" class="product type-product status-publish has-post-thumbnail product_cat-accessories product_brand-global-voices first outofstock featured shipping-taxable purchasable product-type-simple">
                                <div class="product_detail row">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 clear_xs">
                                        <div class="slider_img_productd">
                                            <!-- woocommerce_show_product_images -->
                                            <div id="product_img_01" class="product-images loading" data-rtl="false">
                                                <div class="product-images-container clearfix thumbnail-bottom">
                                                    <!-- Image Slider -->
                                                    <div class="slider product-responsive">
                                                        <div class="item-img-slider">
                                                            <div class="images">
                                                                <a href="{{ asset('public/uploads/product/'.$product->image) }} " data-rel="prettyPhoto[product-gallery]" class="zoom">
                                                                    <img width="600" height="600" src="{{ asset('public/uploads/product/'.$product->image) }}" class="attachment-shop_single size-shop_single" alt="" srcset="{{ asset('public/uploads/product/'.$product->image) }} 600w, {{ asset('public/uploads/product/'.$product->image) }} 150w, {{ asset('public/uploads/product/'.$product->image) }}g 300w, {{ asset('public/uploads/product/'.$product->image) }} 180w" sizes="(max-width: 600px) 100vw, 600px">
                                                                </a>
                                                            </div>
                                                        </div>

                                                        @foreach($product['images'] as $image )
                                                        <div class="item-img-slider">
                                                            <div class="images">
                                                                <a href="{{ asset('public/uploads/product/'.$image->image)  }} " data-rel="prettyPhoto[product-gallery]" class="zoom">
                                                                    <img width="600" height="600" src="{{ asset('public/uploads/product/'.$image->image)  }}" class="attachment-shop_single size-shop_single" alt="" srcset="{{ asset('public/uploads/product/'.$image->image)  }} 600w, {{ asset('public/uploads/product/'.$image->image)  }}150w, {{ asset('public/uploads/product/'.$image->image)  }}300w, {{ asset('public/uploads/product/'.$image->image)  }} 180w" sizes="(max-width: 600px) 100vw, 600px">
                                                                </a>
                                                            </div>
                                                        </div>
                                                            @endforeach


                                                    </div>

                                                    <!-- Thumbnail Slider -->
                                                    <div class="slider product-responsive-thumbnail" id="product_thumbnail_247">

                                                        <div class="item-thumbnail-product">
                                                            <div class="thumbnail-wrapper">
                                                                <img width="180" height="180" src="{{ asset('public/uploads/product/'.$product->image)  }}" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" srcset="{{ asset('public/uploads/product/'.$product->image)  }} 180w, {{ asset('public/uploads/product/'.$product->image)  }} 150w, {{ asset('public/uploads/product/'.$product->image)  }} 300w, {{ asset('public/uploads/product/'.$product->image)  }}600w" sizes="(max-width: 180px) 100vw, 180px">
                                                            </div>
                                                        </div>

                                                        @foreach($product['images'] as $image )

                                                        <div class="item-thumbnail-product">
                                                            <div class="thumbnail-wrapper">
                                                                <img width="180" height="180" src="{{ asset('public/uploads/product/'.$image->image)  }}" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" srcset="{{ asset('public/uploads/product/'.$image->image)  }} 180w, {{ asset('public/uploads/product/'.$image->image)  }} 150w, {{ asset('public/uploads/product/'.$image->image)  }} 300w, {{ asset('public/uploads/product/'.$image->image)  }}600w" sizes="(max-width: 180px) 100vw, 180px">
                                                            </div>
                                                        </div>

                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <form action="{{ route('addToCart') }}" method="post" name="addtoCartForm" id="addtoCart">
                                        @csrf

                                        <input type="hidden" name="product_id" value="{{ $product->id }}">
                                        <input type="hidden" name="product_name" value="{{ $product->product_name }}">
                                        <input type="hidden" name="price" value="{{ $product->price }}">

                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 clear_xs">
                                        <div class="content_product_detail">
                                            <h1 class="product_title entry-title">{{ $product->product_name }}</h1>

                                            <div class="reviews-content">
                                                <div class="star"></div>
                                                <a href="#reviews" class="woocommerce-review-link" rel="nofollow"><span class="count">0</span> Review(s)</a>
                                            </div>


                                            <div>

                                                        <p class="price"><span class="woocommerce-Price-amount amount">
                                                           @if(!empty($product->sale_price))
                                                                    <del style="color: red; font-weight: bold; " class="price">Rs {{ $product->price }} </del>
                                                                &nbsp; &nbsp;
                                                                    <ins class="getPrice price">Rs {{ $product->sale_price }} </ins>
                                                                @else
                                                                    <ins class="getPrice price">Rs {{ $product->price }} </ins>
                                                                @endif
                                                            </span></p>
                                            </div>


                                            <p>
                                                <select name="size" id="selSize">
                                                    <option disabled selected>Select Size</option>
                                                    @foreach($product->attributes as $sizes)
                                                        <option value="{{$product->id}}-{{ $sizes->size }}">{{ $sizes->size }}</option>
                                                    @endforeach
                                                </select>
                                            </p>

                                            <label><strong>Quantity</strong></label>
                                            <input type="number" value="1" class="form-control" name="quantity" style="width: 100px">

                                            <div class="product-info clearfix">
                                                <div class="product-stock pull-left out-stock">
                                                    @if($total_stock > 0)
                                                        <button type="submit" class="btn btn-default cart" id="cartButton">
                                                            <i class="fa fa-shopping-cart"> </i> Add To Cart
                                                        </button>
                                                    @endif


                                                        <span id="availability">
                                                             @if($total_stock > 0) In Stock
                                                             @else
                                                            Out of Stock
                                                            </span>
                                                        @endif
                                                </div>
                                            </div>

                                            <div class="description" itemprop="description">
                                                <p>
                                                    {{ $product->excerpt }}
                                                </p>
                                            </div>

                                            <p class="stock out-of-stock">
                                                @if($total_stock > 0)
                                                         <button type="button" class="btn btn-default cart">
                                                             <i class="fa fa-shopping-cart"> </i> Add To Cart
                                                         </button>
                                                    @endif

                                            </p>

                                            <div class="social-share">
                                                <div class="title-share">Share</div>
                                                <div class="wrap-content">
                                                    <a href="http://www.facebook.com" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-facebook"></i></a>
                                                    <a href="http://twitter.com" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-twitter"></i></a>
                                                    <a href="https://plus.google.com" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-google-plus"></i></a>
                                                    <a href="#"><i class="fa fa-dribbble"></i></a>
                                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    </form>
                                </div>
                            </div>

                            <div class="tabs clearfix">
                                <div class="tabbable">
                                    <ul class="nav nav-tabs">
                                        <li class="description_tab active">
                                            <a href="#tab-description" data-toggle="tab">Description</a>
                                        </li>

                                        <li class="reviews_tab ">
                                            <a href="#tab-reviews" data-toggle="tab">Reviews (0)</a>
                                        </li>
                                    </ul>

                                    <div class="clear"></div>

                                    <div class=" tab-content">
                                        <div class="tab-pane active" id="tab-description">
                                            <h2>Product Description</h2>
                                            {!! $product->description !!}
                                        </div>

                                        <div class="tab-pane " id="tab-reviews">
                                            <div id="reviews">
                                                <div id="comments">
                                                    <h2>Reviews</h2>
                                                    <p class="woocommerce-noreviews">There are no reviews yet.</p>
                                                </div>

                                                <div id="review_form_wrapper">
                                                    <div id="review_form">
                                                        <div id="respond" class="comment-respond">
                                                            <h3 id="reply-title" class="comment-reply-title">
                                                                Be the first to review "turkey qui"
                                                                <small><a rel="nofollow" id="cancel-comment-reply-link" href="#" style="display:none;">Cancel reply</a></small>
                                                            </h3>

                                                            <form action="" method="post" id="commentform" class="comment-form">
                                                                <p class="comment-form-rating">
                                                                    <label for="rating">Your Rating</label>
                                                                    <select name="rating" id="rating">
                                                                        <option value="">Rate ...</option>
                                                                        <option value="5">Perfect</option>
                                                                        <option value="4">Good</option>
                                                                        <option value="3">Average</option>
                                                                        <option value="2">Not that bad</option>
                                                                        <option value="1">Very Poor</option>
                                                                    </select>
                                                                </p>

                                                                <p class="comment-form-comment">
                                                                    <label for="comment">Your Review</label>
                                                                    <textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea>
                                                                </p>

                                                                <p class="form-submit">
                                                                    <input name="submit" type="submit" id="submit" class="submit" value="Submit">
                                                                </p>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="bottom-single-product theme-clearfix">
                                <div class="widget-1 widget-first widget sw_related_upsell_widget-2 sw_related_upsell_widget" data-scroll-reveal="enter bottom move 20px wait 0.2s">
                                    <div class="widget-inner">
                                        <div id="slider_sw_related_upsell_widget-2" class="sw-woo-container-slider related-products responsive-slider clearfix loading" data-lg="4" data-md="3" data-sm="2" data-xs="2" data-mobile="1" data-speed="1000" data-scroll="1" data-interval="5000" data-autoplay="false">
                                            <div class="resp-slider-container">
                                                <div class="box-slider-title">
                                                    <h2><span>Related Products</span></h2>
                                                </div>

                                                <div class="slider responsive">


                                                    @foreach($relatedProducts as $product)
                                                    <div class="item ">
                                                        <div class="item-wrap">
                                                            <div class="item-detail">
                                                                <div class="item-img products-thumb">
                                                                    <a href="{{ route('productDetail', $product->slug) }}">
                                                                        <div class="product-thumb-hover">
                                                                            <img width="300" height="300" src="{{ asset('public/uploads/product/'.$product->image) }}" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="" srcset="{{ asset('public/uploads/product/'.$product->image) }} 300w, {{ asset('public/uploads/product/'.$product->image) }} 150w, {{ asset('public/uploads/product/'.$product->image) }} 180w, {{ asset('public/uploads/product/'.$product->image) }} 600w" sizes="(max-width: 300px) 100vw, 300px">
                                                                        </div>
                                                                    </a>

                                                                    <!-- add to cart, wishlist, compare -->
                                                                    <div class="item-bottom clearfix">
                                                                        <a rel="nofollow" href="#" class="button product_type_simple add_to_cart_button ajax_add_to_cart" title="Add to Cart">Add to cart</a>

                                                                        <a href="javascript:void(0)" class="compare button" rel="nofollow" title="Add to Compare">Compare</a>

                                                                        <div class="yith-wcwl-add-to-wishlist ">
                                                                            <div class="yith-wcwl-add-button show" style="display:block">
                                                                                <a href="wishlist.html" rel="nofollow" class="add_to_wishlist">Add to Wishlist</a>
                                                                                <img src="images/wpspin_light.gif" class="ajax-loading" alt="loading" width="16" height="16" style="visibility:hidden" />
                                                                            </div>

                                                                            <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
                                                                                <span class="feedback">Product added!</span>
                                                                                <a href="#" rel="nofollow">Browse Wishlist</a>
                                                                            </div>

                                                                            <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none">
                                                                                <span class="feedback">The product is already in the wishlist!</span>
                                                                                <a href="#" rel="nofollow">Browse Wishlist</a>
                                                                            </div>

                                                                            <div style="clear:both"></div>
                                                                            <div class="yith-wcwl-wishlistaddresponse"></div>
                                                                        </div>

                                                                        <div class="clear"></div>
                                                                        <a href="ajax/fancybox/example.html" data-fancybox-type="ajax" class="sm_quickview_handler-list fancybox fancybox.ajax">Quick View </a>
                                                                    </div>
                                                                </div>

                                                                <div class="item-content">
                                                                    <!-- rating  -->
                                                                    <div class="reviews-content">
                                                                        <div class="star"></div>
                                                                        <div class="item-number-rating">
                                                                            0 Review(s)
                                                                        </div>
                                                                    </div>
                                                                    <!-- end rating  -->

                                                                    <h4><a href="{{ route('productDetail', $product->slug) }}" title="turkey qui">{{ $product->product_name }}</a></h4>

                                                                    <!-- price -->
                                                                    <div class="item-price">
                                                                        <span>
                                                                            @if(!empty($product->sale_price))
                                                                               Rs.  <del>{{ $product->price }}</del>  &nbsp;
                                                                              Rs. {{ $product->sale_price }}
                                                                                @else
                                                                               Rs. {{ $product->price }}
                                                                                @endif
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="widget-2 widget-last widget sw_related_upsell_widget-3 sw_related_upsell_widget" data-scroll-reveal="enter bottom move 20px wait 0.2s">
                                    <div class="widget-inner"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection

@section('js')
    <script>
        $(document).ready(function (){
            $("#selSize").change(function (){
                var idSize = $(this).val();
                if(idSize == ""){
                    return false;
                }
                $.ajax({
                    headers: {
                      'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'post',
                    url: '{{ route('getProductPrice') }}',
                    data: {idSize:idSize},
                    success: function (resp){
                        var arr = resp.split("#");
                        $(".getPrice").html("Rs. " + arr[0]);
                        $(".price").val(arr[0]);
                        // Send the updated price based on the size of the product
                        if(arr[1] == 0 ){
                            $('#cartButton').hide();
                            $("#availability").text("Out of Stock").css('color', 'red');
                        } else {
                            $('#cartButton').show();
                            $("#availability").text("In Stock").css('color', 'green');
                        }
                    },
                    error: function (resp){
                        alert('Error')
                    }

                })
            });
        });
    </script>
@endsection
