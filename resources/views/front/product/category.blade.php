@extends('front.includes.front_design_v2')

@section('content')

    <div class="listings-title">
        <div class="container">
            <div class="wrap-title">
                <h1>{{ $categoryDetails->category_name }}</h1>

                <div class="bread">
                    <div class="breadcrumbs theme-clearfix">
                        <div class="container">
                            <ul class="breadcrumb">
                                <li>
                                    <a href="{{ route('index') }}">Home</a>
                                    <span class="go-page"></span>
                                </li>

                                <li class="active">
                                    <span>{{ $categoryDetails->category_name }}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div id="contents" class="content col-lg-9 col-md-8 col-sm-8" role="main">


                <div id="container">
                    <div id="content" role="main">
                        <!--  Shop Title -->
                        <div class="products-wrapper">
                            <div class="row-fix clearfix">
                                  @foreach($categories as $cat)
                                <li class="product-category product first product-col-5 col-md-3 col-sm-6 col-xs-6 col-mb-12">
                                    <a href="{{ route('categoryPage', $cat->slug) }}">
                                        @if(!empty($cat->image))
                                            <img src="{{ asset('public/uploads/category/'.$cat->image) }}" width="300" height="300">
                                        @else
                                            <img src="{{ asset('public/uploads/default/cat_image.png') }}" width="300" height="300">
                                        @endif

                                        <h3>
                                            {{ $cat->category_name }}
                                        </h3>
                                    </a>
                                </li>
                                    @endforeach

                            </div>

                            <div class="products-nav clearfix">
                                <div class="view-mode-wrap pull-left clearfix">
                                    <div class="view-mode">
                                        <a href="javascript:void(0)" class="grid-view active" title="Grid view"><span>Grid view</span></a>
                                        <a href="javascript:void(0)" class="list-view" title="List view"><span>List view</span></a>
                                    </div>
                                </div>

                                <div class="catalog-ordering">
                                    <div class="orderby-order-container clearfix">
                                        <ul class="orderby order-dropdown pull-left">
                                            <li>
                                                <span class="current-li"><span class="current-li-content"><a>Sort by Default</a></span></span>
                                                <ul>
                                                    <li class="current"><a href="#">Sort by Default</a></li>
                                                    <li class=""><a href="#">Sort by Popularity</a></li>
                                                    <li class=""><a href="#">Sort by Rating</a></li>
                                                    <li class=""><a href="#">Sort by Date</a></li>
                                                    <li class=""><a href="#">Sort by Price</a></li>
                                                </ul>
                                            </li>
                                        </ul>

                                        <ul class="order pull-left">
                                            <li class="asc"><a href="#"><i class="fa fa-long-arrow-down"></i></a></li>
                                        </ul>

                                        <div class="product-number pull-left clearfix">
                                            <span class="show-product pull-left">Show </span>
                                            <ul class="sort-count order-dropdown pull-left">
                                                <li>
                                                    <span class="current-li"><a>12</a></span>
                                                    <ul>
                                                        <li class="current"><a href="#">12</a></li>
                                                        <li class=""><a href="#">24</a></li>
                                                        <li class=""><a href="#">36</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <nav class="woocommerce-pagination pull-right">
                                    <span class="note">Page:</span>
                                    <ul class="page-numbers">
                                        <li><span class="page-numbers current">1</span></li>
                                        <li><a class="page-numbers" href="#">2</a></li>
                                        <li><a class="page-numbers" href="#">3</a></li>
                                        <li><a class="next page-numbers" href="#">?</a></li>
                                    </ul>
                                </nav>
                            </div>

                            <div class="clear"></div>

                            <ul class="products-loop row grid clearfix">

                                @foreach($productsAll as $product)
                                <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6 post-255 product type-product status-publish has-post-thumbnail product_cat-electronics product_cat-home-appliances product_cat-vacuum-cleaner product_brand-apoteket first instock sale featured shipping-taxable purchasable product-type-simple">
                                    <div class="products-entry item-wrap clearfix">
                                        <div class="item-detail">
                                            <div class="item-img products-thumb">
                                                <span class="onsale">Sale!</span>
                                                <a href="simple_product.html">
                                                    <div class="product-thumb-hover">
                                                        <img 	width="300" height="300" src="{{ asset('public/uploads/product/'.$product->image) }}" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt=""
                                                                srcset="{{ asset('public/uploads/product/'.$product->image) }} 300w, {{ asset('public/uploads/product/'.$product->image) }} 150w, {{ asset('public/uploads/product/'.$product->image) }} 180w, {{ asset('public/uploads/product/'.$product->image) }} 600w"
                                                                sizes="(max-width: 300px) 100vw, 300px" />
                                                    </div>
                                                </a>

                                                <!-- add to cart, wishlist, compare -->
                                                <div class="item-bottom clearfix">
                                                    <a rel="nofollow" href="#" class="button product_type_simple add_to_cart_button ajax_add_to_cart" title="Add to Cart">Add to cart</a>

                                                    <a href="javascript:void(0)" class="compare button" rel="nofollow" title="Add to Compare">Compare</a>

                                                    <div class="yith-wcwl-add-to-wishlist ">
                                                        <div class="yith-wcwl-add-button show" style="display:block">
                                                            <a href="wishlist.html" rel="nofollow" class="add_to_wishlist">Add to Wishlist</a>
                                                            <img src="images/wpspin_light.gif" class="ajax-loading" alt="loading" width="16" height="16" style="visibility:hidden" />
                                                        </div>

                                                        <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
                                                            <span class="feedback">Product added!</span>
                                                            <a href="#" rel="nofollow">Browse Wishlist</a>
                                                        </div>

                                                        <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none">
                                                            <span class="feedback">The product is already in the wishlist!</span>
                                                            <a href="#" rel="nofollow">Browse Wishlist</a>
                                                        </div>

                                                        <div style="clear:both"></div>
                                                        <div class="yith-wcwl-wishlistaddresponse"></div>
                                                    </div>

                                                    <div class="clear"></div>
                                                    <a href="ajax/fancybox/example.html" data-fancybox-type="ajax" class="sm_quickview_handler-list fancybox fancybox.ajax">Quick View </a>
                                                </div>
                                            </div>

                                            <div class="item-content products-content">
                                                <div class="reviews-content">
                                                    <div class="star"><span style="width: 63px"></span></div>
                                                </div>

                                                <h4>
                                                    <a href="{{ route('productDetail', $product->slug) }}" title="{{ $product->product_name }}">
                                                        {{ $product->product_name }}
                                                    </a>
                                                </h4>

                                                <span class="item-price">
                                                    @if(!empty($product->sale_price))
                                                    <del>
                                                        <span class="woocommerce-Price-amount amount">
                                                            <span class="woocommerce-Price-currencySymbol">
                                                                Rs
                                                          </span>
                                                            {{ $product->price }}
                                                        </span>
                                                    </del>
                                                    <ins><span class="woocommerce-Price-amount amount">
                                                            <span class="woocommerce-Price-currencySymbol">Rs </span>
                                                         {{ $product->sale_price }}</span>
                                                    </ins>
                                                    @else
                                                        <ins><span class="woocommerce-Price-amount amount">
                                                            <span class="woocommerce-Price-currencySymbol">Rs </span>{{ $product->price }}</span>
                                                    </ins>
                                                        @endif
                                                </span>




                                                <div class="item-description">Proin nunc nibh, adipiscing eu nisi id, ultrices suscipit augue. Sed rhoncus hendrerit lacus, et venenatis felis. Donec ut fringilla magna ultrices suscipit augue. Proin nunc nibh, adipiscing eu nisi id, ultrices suscipit augue. Sed rhoncus hendrerit lacus, et venenatis felis. Donec ut fringilla magna ultrices suscipit augue.</div>

                                                <!-- add to cart, wishlist, compare -->
                                                <div class="item-bottom clearfix">
                                                    <a rel="nofollow" href="#" class="button product_type_simple add_to_cart_button ajax_add_to_cart" title="Add to Cart">Add to cart</a>

                                                    <a href="javascript:void(0)" class="compare button" rel="nofollow" title="Add to Compare">Compare</a>

                                                    <div class="yith-wcwl-add-to-wishlist ">
                                                        <div class="yith-wcwl-add-button show" style="display:block">
                                                            <a href="wishlist.html" rel="nofollow" class="add_to_wishlist">Add to Wishlist</a>
                                                            <img src="images/wpspin_light.gif" class="ajax-loading" alt="loading" width="16" height="16" style="visibility:hidden" />
                                                        </div>

                                                        <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
                                                            <span class="feedback">Product added!</span>
                                                            <a href="#" rel="nofollow">Browse Wishlist</a>
                                                        </div>

                                                        <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none">
                                                            <span class="feedback">The product is already in the wishlist!</span>
                                                            <a href="#" rel="nofollow">Browse Wishlist</a>
                                                        </div>

                                                        <div style="clear:both"></div>
                                                        <div class="yith-wcwl-wishlistaddresponse"></div>
                                                    </div>

                                                    <div class="clear"></div>
                                                    <a href="ajax/fancybox/example.html" data-fancybox-type="ajax" class="sm_quickview_handler-list fancybox fancybox.ajax">Quick View </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                            </ul>

                            <div class="clear"></div>


                        </div>
                    </div>
                </div>
            </div>

            <aside id="left" class="sidebar col-lg-3 col-md-4 col-sm-4">
                <div class="widget-1 widget-first widget woocommerce_product_categories-3 woocommerce widget_product_categories">
                    <div class="widget-inner">
                        <div class="block-title-widget">
                            <h2><span>Categories</span></h2>
                        </div>

                        <ul class="product-categories">
                            <li class="cat-item"><a href="shop.html">Accessories</a> <span class="count">(2)</span></li>
                            <li class="cat-item"><a href="shop.html">Accessories</a> <span class="count">(1)</span></li>
                            <li class="cat-item"><a href="shop.html">Accessories for Tablet</a> <span class="count">(2)</span></li>
                            <li class="cat-item"><a href="shop.html">Air Conditional</a> <span class="count">(0)</span></li>
                            <li class="cat-item"><a href="shop.html">Appliances</a> <span class="count">(6)</span></li>
                            <li class="cat-item"><a href="shop.html">Batteries &amp; Chargers</a> <span class="count">(1)</span></li>
                            <li class="cat-item"><a href="shop.html">Blender</a> <span class="count">(2)</span></li>
                            <li class="cat-item"><a href="shop.html">Cameras &amp; Accessories</a> <span class="count">(3)</span></li>
                            <li class="cat-item"><a href="shop.html">Cameras &amp; Camcorders</a> <span class="count">(2)</span></li>
                            <li class="cat-item"><a href="shop.html">Computers &amp; Laptops</a> <span class="count">(6)</span></li>
                            <li class="cat-item"><a href="shop.html">Computers &amp; Networking</a> <span class="count">(1)</span></li>
                            <li class="cat-item"><a href="shop.html">Electronic Component</a> <span class="count">(0)</span></li>
                            <li class="cat-item"><a href="shop.html">Electronics</a> <span class="count">(8)</span></li>
                            <li class="cat-item"><a href="shop.html">Headphone</a> <span class="count">(1)</span></li>
                            <li class="cat-item"><a href="shop.html">Home Appliances</a> <span class="count">(1)</span></li>
                            <li class="cat-item"><a href="shop.html">Home Furniture</a> <span class="count">(1)</span></li>
                            <li class="cat-item"><a href="shop.html">Household Goods</a> <span class="count">(0)</span></li>
                            <li class="cat-item"><a href="shop.html">Laptop Asus</a> <span class="count">(1)</span></li>
                            <li class="cat-item"><a href="shop.html">Laptop Dell</a> <span class="count">(1)</span></li>
                            <li class="cat-item"><a href="shop.html">Laptop HP</a> <span class="count">(0)</span></li>
                            <li class="cat-item"><a href="shop.html">Laptop MSI</a> <span class="count">(0)</span></li>
                            <li class="cat-item"><a href="shop.html">Laptops &amp; Accessories</a> <span class="count">(2)</span></li>
                            <li class="cat-item"><a href="shop.html">Macbook</a> <span class="count">(1)</span></li>
                            <li class="cat-item"><a href="shop.html">Microwave</a> <span class="count">(1)</span></li>
                            <li class="cat-item"><a href="shop.html">Mixer</a> <span class="count">(1)</span></li>
                            <li class="cat-item"><a href="shop.html">Mp3 Player Accessories</a> <span class="count">(1)</span></li>
                            <li class="cat-item"><a href="shop.html">Paper Towel</a> <span class="count">(0)</span></li>
                            <li class="cat-item"><a href="shop.html">Smartphones &amp; Tablet</a> <span class="count">(2)</span></li>
                            <li class="cat-item"><a href="shop.html">Sponge</a> <span class="count">(0)</span></li>
                            <li class="cat-item"><a href="shop.html">Television</a> <span class="count">(2)</span></li>
                            <li class="cat-item"><a href="shop.html">Television</a> <span class="count">(1)</span></li>
                            <li class="cat-item"><a href="shop.html">Televisions</a> <span class="count">(2)</span></li>
                            <li class="cat-item"><a href="shop.html">Vacuum Cleaner</a> <span class="count">(2)</span></li>
                        </ul>
                    </div>
                </div>

                <div class="widget-2 widget woocommerce_layered_nav-4 woocommerce widget_layered_nav">
                    <div class="widget-inner">
                        <div class="block-title-widget">
                            <h2><span>Colors</span></h2>
                        </div>

                        <ul>
                            <li class="wc-layered-nav-term "><a href="shop.html">Black</a> <span class="count">(3)</span></li>
                            <li class="wc-layered-nav-term "><a href="shop.html">Blue</a> <span class="count">(2)</span></li>
                            <li class="wc-layered-nav-term "><a href="shop.html">Orange</a> <span class="count">(1)</span></li>
                            <li class="wc-layered-nav-term "><a href="shop.html">White</a> <span class="count">(3)</span></li>
                            <li class="wc-layered-nav-term "><a href="shop.html">Yellow</a> <span class="count">(1)</span></li>
                        </ul>
                    </div>
                </div>

                <div class="widget-3 widget woocommerce_layered_nav-5 woocommerce widget_layered_nav">
                    <div class="widget-inner">
                        <div class="block-title-widget">
                            <h2><span>Size</span></h2>
                        </div>

                        <ul>
                            <li class="wc-layered-nav-term "><a href="shop.html">L</a> <span class="count">(3)</span></li>
                            <li class="wc-layered-nav-term "><a href="shop.html">M</a> <span class="count">(1)</span></li>
                            <li class="wc-layered-nav-term "><a href="shop.html">S</a> <span class="count">(2)</span></li>
                            <li class="wc-layered-nav-term "><a href="shop.html">XL</a> <span class="count">(3)</span></li>
                            <li class="wc-layered-nav-term "><a href="shop.html">XS</a> <span class="count">(1)</span></li>
                        </ul>
                    </div>
                </div>

                <div class="widget-4 widget woocommerce_price_filter-3 woocommerce widget_price_filter">
                    <div class="widget-inner">
                        <div class="block-title-widget">
                            <h2><span>price</span></h2>
                        </div>

                        <form method="get" action="">
                            <div class="price_slider_wrapper">
                                <div class="price_slider" style="display:none;"></div>
                                <div class="price_slider_amount">
                                    <input type="text" id="min_price" name="min_price" value="" data-min="150" placeholder="Min price">
                                    <input type="text" id="max_price" name="max_price" value="" data-max="700" placeholder="Max price">

                                    <button type="button" class="button">Filter</button>

                                    <div class="price_label" style="display:none;">
                                        Price: <span class="from"></span> — <span class="to"></span>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="widget-5 widget etrostore_best_seller_product-3 etrostore_best_seller_product">
                    <div class="widget-inner">
                        <div class="block-title-widget">
                            <h2><span>Best Sellers</span></h2>
                        </div>

                        <div id="best-seller-01" class="sw-best-seller-product">
                            <ul class="list-unstyled">
                                <li class="clearfix">
                                    <div class="item-img">
                                        <a href="simple_product.html" title="corned beef enim">
                                            <img width="180" height="180" src="images/1903/65-180x180.jpg" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="" srcset="images/1903/65-180x180.jpg 180w, images/1903/65-150x150.jpg 150w, images/1903/65-300x300.jpg 300w, images/1903/65.jpg 600w" sizes="(max-width: 180px) 100vw, 180px">
                                        </a>
                                    </div>

                                    <div class="item-content">
                                        <div class="reviews-content">
                                            <div class="star"></div>
                                            <div class="item-number-rating">
                                                0 Review(s)
                                            </div>
                                        </div>

                                        <h4><a href="simple_product.html" title="corned beef enim">Corned beef enim</a></h4>

                                        <div class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>400.00</span></div>
                                    </div>
                                </li>

                                <li class="clearfix">
                                    <div class="item-img">
                                        <a href="simple_product.html" title="philips stand">
                                            <img width="180" height="180" src="images/1903/62-180x180.jpg" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="" srcset="images/1903/62-180x180.jpg 180w, images/1903/62-150x150.jpg 150w, images/1903/62-300x300.jpg 300w, images/1903/62.jpg 600w" sizes="(max-width: 180px) 100vw, 180px">
                                        </a>
                                    </div>

                                    <div class="item-content">
                                        <div class="reviews-content">
                                            <div class="star"></div>
                                            <div class="item-number-rating">
                                                0 Review(s)
                                            </div>
                                        </div>

                                        <h4><a href="simple_product.html" title="philips stand">Philips stand</a></h4>

                                        <div class="price"><del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>300.00</span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>250.00</span></ins></div>
                                    </div>
                                </li>

                                <li class="clearfix">
                                    <div class="item-img">
                                        <a href="simple_product.html" title="Vacuum cleaner">
                                            <img width="180" height="180" src="images/1903/26-180x180.jpg" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="" srcset="images/1903/26-180x180.jpg 180w, images/1903/26-150x150.jpg 150w, images/1903/26-300x300.jpg 300w, images/1903/26.jpg 600w" sizes="(max-width: 180px) 100vw, 180px">
                                        </a>
                                    </div>

                                    <div class="item-content">
                                        <div class="reviews-content">
                                            <div class="star"><span style="width:52.5px"></span></div>
                                            <div class="item-number-rating">
                                                4 Review(s)
                                            </div>
                                        </div>

                                        <h4><a href="simple_product.html" title="Vacuum cleaner">Vacuum cleaner</a></h4>

                                        <div class="price"><del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>350.00</span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>260.00</span></ins></div>
                                    </div>
                                </li>

                                <li class="clearfix">
                                    <div class="item-img">
                                        <a href="simple_product.html" title="veniam dolore">
                                            <img width="180" height="180" src="images/1903/45-180x180.jpg" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="" srcset="images/1903/45-180x180.jpg 180w, images/1903/45-150x150.jpg 150w, images/1903/45-300x300.jpg 300w, images/1903/45.jpg 600w" sizes="(max-width: 180px) 100vw, 180px">
                                        </a>
                                    </div>

                                    <div class="item-content">
                                        <div class="reviews-content">
                                            <div class="star"><span style="width:35px"></span></div>
                                            <div class="item-number-rating">
                                                2 Review(s)
                                            </div>
                                        </div>

                                        <h4><a href="simple_product.html" title="veniam dolore">Veniam dolore</a></h4>

                                        <div class="price"><del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>250.00</span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>190.00</span></ins></div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="widget-6 widget-last widget text-6 widget_text">
                    <div class="widget-inner">
                        <div class="textwidget">
                            <div class="banner-sidebar">
                                <img src="images/1903/banner-detail.jpg" title="banner" alt="banner">
                            </div>
                        </div>
                    </div>
                </div>
            </aside>
        </div>
    </div>

    @endsection
