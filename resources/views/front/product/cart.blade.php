@extends('front.includes.front_design_v2')

@section('content')
    <div class="listings-title">
        <div class="container">
            <div class="wrap-title">
                <h1>Cart</h1>
                <div class="bread">
                    <div class="breadcrumbs theme-clearfix">
                        <div class="container">
                            <ul class="breadcrumb">
                                <li>
                                    <a href="home_page_1.html">Home</a>
                                    <span class="go-page"></span>
                                </li>

                                <li class="active">
                                    <span>Cart</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if(Session::has('flash_message_info'))
        <div class="alert alert-primary text-center" role="alert" style="color: blue;">
            {{ Session::get('flash_message_info') }}
        </div>
    @endif

    @if(Session::has('flash_message_error'))
        <div class="alert alert-primary text-center" role="alert" style="color: red;">
            {{ Session::get('flash_message_error') }}
        </div>
    @endif

    <div class="container">
        <div class="row">
            <div id="contents" role="main" class="main-page col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="page type-page status-publish hentry">
                    <div class="entry-content">
                        <div class="entry-summary">





                            <div class="woocommerce">
                                <form action="" method="post">

                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif



                                    @if(Session::has('flash_message_info'))
                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            {{ Session::get('flash_message_info') }}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    @endif

                                    @if(Session::has('error_message'))
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            {{ Session::get('error_message') }}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    @endif



                                    <table class="shop_table shop_table_responsive cart" cellspacing="0">
                                        <thead>
                                        <tr>
                                            <th class="product-remove">&nbsp;</th>
                                            <th class="product-thumbnail">&nbsp;</th>
                                            <th class="product-name">Product</th>
                                            <th class="product-price">Price</th>
                                            <th class="product-quantity">Quantity</th>
                                            <th class="product-subtotal">Total</th>
                                        </tr>
                                        </thead>
                                        @php $total_amount = 0; @endphp
                                        <tbody>
                                        @foreach($userCart as $cart)
                                        <tr class="cart_item">
                                            <td class="product-remove">
                                                <a href="{{ route('deleteCart', $cart->id) }}" class="remove" title="Remove this item"><i class="fa fa-times" aria-hidden="true"></i></a>
                                            </td>

                                            <td class="product-thumbnail">
                                                <a href="simple_product.html">
                                                    <img width="180" height="180" src="{{ asset('public/uploads/product/'.$cart->image) }}" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="" srcset="{{ asset('public/uploads/product/'.$cart->image) }} 180w, {{ asset('public/uploads/product/'.$cart->image) }} 150w, {{ asset('public/uploads/product/'.$cart->image) }} 300w, {{ asset('public/uploads/product/'.$cart->image) }} 600w" sizes="(max-width: 180px) 100vw, 180px">
                                                </a>
                                            </td>

                                            <td class="product-name" data-title="Product">
                                                <a href="simple_product.html">
                                                    {{ $cart->product_name }}
                                                </a>
                                            </td>

                                            <td class="product-price" data-title="Price">
                                                <span class="woocommerce-Price-amount amount">
                                                    Rs. {{ $cart->price }}
                                                </span>
                                            </td>

                                            <td class="product-quantity" data-title="Quantity">
                                                <a href="{{ url('/cart/update-quantity/'.$cart->id.'/1') }}" class="cart_quantity_up"> + </a>

                                                <div class="quantity">
                                                    <input type="number" step="1" min="0" max="" id="cart_quantity_input" name="quantity" value="{{ $cart->quantity }}" title="Qty" class="input-text qty text" size="4" pattern="[0-9]*" inputmode="numeric">
                                                </div>
                                                @if($cart->quantity > 1)
                                                <a href="{{ url('/cart/update-quantity/'.$cart->id.'/-1') }}" class="cart_quantity_down"> - </a>
                                                    @endif

                                            </td>

                                            <td class="product-subtotal" data-title="Total">
                                                <span class="woocommerce-Price-amount amount">
                                                    Rs. {{ $cart->price * $cart->quantity }}
                                                </span>
                                            </td>
                                        </tr>

                                            @php $total_amount = $total_amount + ($cart->price * $cart->quantity) @endphp
                                        @endforeach
                                </form>

                                        <tr>

                                            <td colspan="6" class="actions">
                                                <div class="coupon">
                                                    <form action="{{ route('applyCoupon') }}" method="post">
                                                        @csrf
                                                    <label for="coupon_code">Coupon:</label>
                                                    <input type="text" name="coupon_code" id="coupon_code" class="input-text"  placeholder="Coupon code">
                                                    <button type="submit" class="button"> Apply Coupon</button>
                                                    </form>

                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                <div class="cart-collaterals">
                                    <div class="products-wrapper">
                                        <div class="cart_totals ">
                                            <h2>Cart Totals</h2>

                                            <table cellspacing="0" class="shop_table shop_table_responsive">
                                                @if(!empty(Session::get('CouponAmount')))
                                                <tbody>
                                                <tr class="cart-subtotal">
                                                    <th>Subtotal</th>
                                                    <td data-title="Subtotal">
                                                        <span class="woocommerce-Price-amount amount">Rs. <?php echo $total_amount;  ?></span>
                                                    </td>
                                                </tr>

                                                <tr class="cart-subtotal">
                                                    <th>Discount Amount</th>
                                                    <td data-title="Subtotal">
                                                        <span class="woocommerce-Price-amount amount">Rs. {{ Session::get('CouponAmount') }}</span>
                                                    </td>
                                                </tr>


                                                <tr class="order-total">
                                                    <th>Grand Total</th>
                                                    <td data-title="Total">
                                                        <strong><span class="woocommerce-Price-amount amount">Rs. <?php echo $total_amount - Session::get('CouponAmount') ?> </span></strong>
                                                    </td>
                                                </tr>
                                                </tbody>
                                                @else
                                                    <tbody>
                                                    <tr class="cart-subtotal">
                                                        <th>Total Amount</th>
                                                        <td data-title="Subtotal">
                                                            <span class="woocommerce-Price-amount amount">Rs. <?php echo $total_amount;  ?></span>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                @endif
                                            </table>

                                            <div class="wc-proceed-to-checkout">
                                                <a href="checkout.html" class="checkout-button button alt wc-forward">Proceed to Checkout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
