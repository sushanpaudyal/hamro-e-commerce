@extends('front.includes.front_design_v2')

@section('content')

    <div class="listings-title">
        <div class="container">
            <div class="wrap-title">
                <h1>{{ $title }}</h1>

                <div class="bread">
                    <div class="breadcrumbs theme-clearfix">
                        <div class="container">
                            <ul class="breadcrumb">
                                <li>
                                    <a href="{{ route('index') }}">Home</a>
                                    <span class="go-page"></span>
                                </li>

                                <li class="active">
                                    <span>{{ $title }}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div id="contents" role="main" class="main-page col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="post-6 page type-page status-publish hentry">
                    <div class="entry-content">
                        <div class="entry-summary">
                            <div id="sw_deal_01" class="sw-hotdeal ">
                                <div class="sw-hotdeal-content">

                                    @foreach($featuredProducts as $product)
                                    <div class="item-product col-lg-3 col-md-3 col-sm-4 col-xs-6">
                                        <div class="item-detail">
                                            <div class="item-img products-thumb">
                                                <span class="onsale">Sale!</span>
                                                <a href="{{ route('productDetail', $product->slug) }}">
                                                    <div class="product-thumb-hover"><img width="300" height="300" src="{{ asset('public/uploads/product/'.$product->image) }}" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="" srcset="{{ asset('public/uploads/product/'.$product->image) }} 300w, {{ asset('public/uploads/product/'.$product->image) }} 150w, {{ asset('public/uploads/product/'.$product->image) }} 180w, {{ asset('public/uploads/product/'.$product->image) }} 600w" sizes="(max-width: 300px) 100vw, 300px"></div>
                                                </a>

                                                <!-- add to cart, wishlist, compare -->
                                                <div class="item-bottom clearfix">
                                                    <a rel="nofollow" href="#" class="button product_type_simple add_to_cart_button ajax_add_to_cart" title="Add to Cart">Add to cart</a>

                                                    <a href="javascript:void(0)" class="compare button" rel="nofollow" title="Add to Compare">Compare</a>

                                                    <div class="yith-wcwl-add-to-wishlist ">
                                                        <div class="yith-wcwl-add-button show" style="display:block">
                                                            <a href="wishlist.html" rel="nofollow" class="add_to_wishlist">Add to Wishlist</a>
                                                            <img src="images/wpspin_light.gif" class="ajax-loading" alt="loading" width="16" height="16" style="visibility:hidden" />
                                                        </div>

                                                        <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
                                                            <span class="feedback">Product added!</span>
                                                            <a href="#" rel="nofollow">Browse Wishlist</a>
                                                        </div>

                                                        <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none">
                                                            <span class="feedback">The product is already in the wishlist!</span>
                                                            <a href="#" rel="nofollow">Browse Wishlist</a>
                                                        </div>

                                                        <div style="clear:both"></div>
                                                        <div class="yith-wcwl-wishlistaddresponse"></div>
                                                    </div>

                                                    <div class="clear"></div>
                                                </div>

                                                <?php
                                                if(!empty($product->sale_price)){
                                                    $discount_amount = $product->price - $product->sale_price;
                                                } else {
                                                    $discount_amount = 0;
                                                }

                                                $percentage = ($discount_amount/ $product->price) * 100;
                                                if($percentage!=0)
                                                {
                                                ?>

                                                <div class="sale-off">
                                                    @php
                                                        echo floor($percentage) . "%";
                                                    @endphp
                                                </div>
                                                <?php } ?>
                                            </div>

                                            <div class="item-content">
                                                <!-- rating  -->
                                                <div class="reviews-content">
                                                    <div class="star"></div>
                                                    <div class="item-number-rating">
                                                        0 Review(s)
                                                    </div>
                                                </div>
                                                <!-- end rating  -->

                                                <h4><a href="{{ route('productDetail', $product->slug) }}" title="{{ $product->product_name }}">
                                                        {{ $product->product_name }}
                                                    </a></h4>

                                                <!-- price -->
                                                <div class="item-price">
                                                    @if(!empty($product->sale_price))
                                                        <del>
                                                            Rs. {{ $product->price }}
                                                        </del>fTest
                                                        <ins>
                                                            Rs. {{ $product->sale_price }}

                                                        </ins>
                                                    @else
                                                        <ins>
                                                            Rs. {{ $product->price }}
                                                        </ins>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach


                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection
