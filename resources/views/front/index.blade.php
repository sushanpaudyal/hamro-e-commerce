@extends('front.includes.front_design')

@section('content')
    <div class="listings-title">
        <div class="container">
            <div class="wrap-title">
                <h1>Home</h1>

                <div class="bread">
                    <div class="breadcrumbs theme-clearfix">
                        <div class="container">
                            <ul class="breadcrumb">
                                <li>
                                    <a href="home_page_1.html">Home</a>
                                    <span class="go-page"></span>
                                </li>

                                <li class="active">
                                    <span>Home</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div id="contents" role="main" class="main-page  col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="post-6 page type-page status-publish hentry">
                    <div class="entry-content">
                        <div class="entry-summary">
                            <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row vc_row-fluid bg-wrap homepage1_custom vc_row-no-padding">
                                <div class="container float vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="vc_row vc_inner vc_row-fluid ">
                                                <div class="wrap-vertical vc_column_container vc_col-sm-2">
                                                    <div class="vc_column-inner ">
                                                        <div class="wpb_wrapper">
                                                            <div class="vc_wp_custommenu wpb_content_element wrap-title">
                                                                <div class="mega-left-title">
                                                                    <strong>Category</strong>
                                                                </div>

                                                                <div class="wrapper_vertical_menu vertical_megamenu">
                                                                    <div class="resmenu-container">
                                                                        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#ResMenuvertical_menu">
                                                                            <span class="sr-only">Categories</span>
                                                                            <span class="icon-bar"></span>
                                                                            <span class="icon-bar"></span>
                                                                            <span class="icon-bar"></span>
                                                                        </button>

                                                                        <div id="ResMenuvertical_menu" class="collapse menu-responsive-wrapper">
                                                                            <ul id="menu-vertical-menu" class="etrostore_resmenu">
                                                                                @foreach($mainCategories as $category)
                                                                                    <li class="menu-television">
                                                                                        <a class="item-link" href="{{ route('categoryPage', $category->slug) }}">
                                                                                            {{ $category->category_name }}
                                                                                        </a>
                                                                                    </li>
                                                                                @endforeach
                                                                            </ul>
                                                                        </div>
                                                                    </div>

                                                                    <ul id="menu-vertical-menu-1" class="nav vertical-megamenu etrostore-mega etrostore-menures">

                                                                        @foreach($mainCategories as $category)
                                                                        <li class="menu-television">
                                                                            <a class="item-link" href="{{ route('categoryPage', $category->slug) }}">
                                                                                {{ $category->category_name }}
                                                                            </a>
                                                                        </li>
                                                                            @endforeach
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="wrap-slider vc_column_container vc_col-sm-8">
                                                    <div class="vc_column-inner">
                                                        <div class="wpb_wrapper">
                                                            <!-- OWL SLIDER -->
                                                            <div class="wpb_revslider_element wpb_content_element no-margin">
                                                                <div class="vc_column-inner ">
                                                                    <div class="wpb_wrapper">
                                                                        <div class="wpb_revslider_element wpb_content_element">
                                                                            <div id="main-slider" class="fullwidthbanner-container" style="position:relative; width:100%; height:auto; margin-top:0px; margin-bottom:0px">
                                                                                <div class="module slideshow no-margin">
                                                                                    <div class="item">
                                                                                        <a href="simple_product.html"><img src="public/frontend/images/1903/slider2.jpg" alt="slider1" class="img-responsive" height="559"></a>
                                                                                    </div>
                                                                                    <div class="item">
                                                                                        <a href="simple_product.html"><img src="public/frontend/images/1903/01_index_v1.jpg" alt="slider2" class="img-responsive" height="559"></a>
                                                                                    </div>
                                                                                    <div class="item">
                                                                                        <a href="simple_product.html"><img src="public/frontend/images/1903/slider3.jpg" alt="slider3" class="img-responsive" height="559"></a>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="loadeding"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- OWL LIGHT SLIDER -->

                                                            <div class=" wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="banner">
                                                                        <a href="#" class="banner1">
                                                                            <img src="public/frontend/images/1903/banner3.jpg" alt="banner" title="banner" />
                                                                        </a>

                                                                        <a href="#" class="banner2">
                                                                            <img src="public/frontend/images/1903/banner4.jpg" alt="banner" title="banner" />
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="wrap-banner vc_column_container vc_col-sm-2">
                                                    <div class="vc_column-inner ">
                                                        <div class="wpb_wrapper">
                                                            <div class="wpb_single_image wpb_content_element vc_align_center ">
                                                                <figure class="wpb_wrapper vc_figure">
                                                                    <a href="#" target="_self" class="vc_single_image-wrapper vc_box_border_grey">
                                                                        <img class="vc_single_image-img" src="public/frontend/images/1903/banner1.jpg" width="193" height="352" alt="banner1" title="banner1" />
                                                                    </a>
                                                                </figure>
                                                            </div>

                                                            <div class="wpb_single_image wpb_content_element vc_align_center">
                                                                <figure class="wpb_wrapper vc_figure">
                                                                    <a href="#" target="_self" class="vc_single_image-wrapper vc_box_border_grey">
                                                                        <img class="vc_single_image-img" src="public/frontend/images/1903/banner2.jpg" width="193" height="175" alt="banner2" title="banner2" />
                                                                    </a>
                                                                </figure>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class=" wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <div class="wrap-transport">
                                                        <div class="row">
                                                            <div class="item item-1 col-lg-3 col-md-3 col-sm-6">
                                                                <a href="#" class="wrap">
                                                                    <div class="icon">
                                                                        <i class="fa fa-paper-plane"></i>
                                                                    </div>

                                                                    <div class="content">
                                                                        <h3>Money Back Guarantee</h3>
                                                                        <p>30 Days Money Back</p>
                                                                    </div>
                                                                </a>
                                                            </div>

                                                            <div class="item item-2 col-lg-3 col-md-3 col-sm-6">
                                                                <a href="#" class="wrap">
                                                                    <div class="icon">
                                                                        <i class="fa fa-map-marker"></i>
                                                                    </div>

                                                                    <div class="content">
                                                                        <h3>Free Worldwide Shipping</h3>
                                                                        <p>On Order Over $100</p>
                                                                    </div>
                                                                </a>
                                                            </div>

                                                            <div class="item item-3 col-lg-3 col-md-3 col-sm-6">
                                                                <a href="#" class="wrap">
                                                                    <div class="icon">
                                                                        <i class="fa fa-tag"></i>
                                                                    </div>

                                                                    <div class="content">
                                                                        <h3>Member Discount</h3>
                                                                        <p>Upto 70 % Discount</p>
                                                                    </div>
                                                                </a>
                                                            </div>

                                                            <div class="item item-4 col-lg-3 col-md-3 col-sm-6">
                                                                <a href="#" class="wrap">
                                                                    <div class="icon">
                                                                        <i class="fa fa-life-ring"></i>
                                                                    </div>

                                                                    <div class="content">
                                                                        <h3>24/7 Online Support</h3>
                                                                        <p>Technical Support 24/7</p>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="vc_row-full-width vc_clearfix"></div>

                            <div class="vc_row vc_row-fluid margin-bottom-60">
                                <div class="vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div id="_sw_countdown_01" class="sw-woo-container-slider responsive-slider countdown-slider" data-lg="5" data-md="4" data-sm="2" data-xs="1" data-mobile="1" data-speed="1000" data-scroll="1" data-interval="5000" data-autoplay="false" data-circle="false">
                                                <div class="resp-slider-container">
                                                    <div class="box-title">
                                                        <h3>Featured Products</h3>
                                                        <a href="{{ route('featuredProducts') }}">See all</a>
                                                    </div>

                                                    <div class="banner-content">
                                                        <img 	width="195" height="354" src="public/frontend/images/1903/image-cd.jpg" class="attachment-larges size-larges" alt=""
                                                                srcset="public/frontend/images/1903/image-cd.jpg 195w, public/frontend/images/1903/image-cd-165x300.jpg 165w"
                                                                sizes="(max-width: 195px) 100vw, 195px" />
                                                    </div>

                                                    <div class="slider responsive">
                                                        @foreach($featuredProducts as $product)
                                                            <div class="item-countdown product " id="product_sw_countdown_02">
                                                                <div class="item-wrap">
                                                                    <div class="item-detail">
                                                                        <div class="item-content">
                                                                            <!-- rating  -->
                                                                            <div class="reviews-content">
                                                                                <div class="star">
                                                                                    <span style="width:35px"></span>
                                                                                </div>

                                                                                <div class="item-number-rating">2 Review(s)</div>
                                                                            </div>
                                                                            <!-- end rating  -->

                                                                            <h4>
                                                                                <a href="{{ route('productDetail', $product->slug) }}" title="{{ $product->product_name }}">
                                                                                    {{ $product->product_name }}
                                                                                </a>
                                                                            </h4>

                                                                            <!-- Price -->
                                                                            <div class="item-price">
                                                                                @if(!empty($product->sale_price))
                                                                                    <del>
                                                                                        Rs. {{ $product->price }}
                                                                                    </del>
                                                                                    <ins>
                                                                                        Rs. {{ $product->sale_price }}

                                                                                    </ins>
                                                                                @else
                                                                                    <ins>
                                                                                        Rs. {{ $product->price }}
                                                                                    </ins>
                                                                                @endif
                                                                            </div>
                                                                            <?php
                                                                                if(!empty($product->sale_price)){
                                                                                            $discount_amount = $product->price - $product->sale_price;
                                                                                              } else {
                                                                                                $discount_amount = 0;
                                                                                              }

                                                                                      $percentage = ($discount_amount/ $product->price) * 100;
                                                                                if($percentage!=0)
                                                                                 {
                                                                            ?>

                                                                            <div class="sale-off">
                                                                                @php
                                                                                    echo floor($percentage) . "%";
                                                                                @endphp
                                                                            </div>
                                                                            <?php } ?>

                                                                        </div>

                                                                        <div class="item-image-countdown">
                                                                            <span class="onsale">Sale!</span>

                                                                            <a href="{{ route('productDetail', $product->slug) }}">
                                                                                <div class="product-thumb-hover">
                                                                                    <img 	width="300" height="300" src="{{ asset('public/uploads/product/'.$product->image) }}" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt=""
                                                                                            srcset="{{ asset('public/uploads/product/'.$product->image) }} 300w, {{ asset('public/uploads/product/'.$product->image) }} 150w, {{ asset('public/uploads/product/'.$product->image) }} 180w, {{ asset('public/uploads/product/'.$product->image) }} 600w"
                                                                                            sizes="(max-width: 300px) 100vw, 300px" />
                                                                                </div>
                                                                            </a>

                                                                            <!-- add to cart, wishlist, compare -->
                                                                            <div class="item-bottom">
                                                                                <a rel="nofollow" href="#" class="button product_type_simple add_to_cart_button ajax_add_to_cart" title="Add to Cart">Add to cart</a>

                                                                                <a href="javascript:void(0)" class="compare button" rel="nofollow" title="Add to Compare">Compare</a>

                                                                                <div class="yith-wcwl-add-to-wishlist ">
                                                                                    <div class="yith-wcwl-add-button show" style="display:block">
                                                                                        <a href="wishlist.html" rel="nofollow" class="add_to_wishlist">Add to Wishlist</a>
                                                                                        <img src="public/frontend/images/wpspin_light.gif" class="ajax-loading" alt="loading" width="16" height="16" style="visibility:hidden" />
                                                                                    </div>

                                                                                    <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
                                                                                        <span class="feedback">Product added!</span>
                                                                                        <a href="#" rel="nofollow">Browse Wishlist</a>
                                                                                    </div>

                                                                                    <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none">
                                                                                        <span class="feedback">The product is already in the wishlist!</span>
                                                                                        <a href="#" rel="nofollow">Browse Wishlist</a>
                                                                                    </div>

                                                                                    <div style="clear:both"></div>
                                                                                    <div class="yith-wcwl-wishlistaddresponse"></div>
                                                                                </div>

                                                                                <div class="clear"></div>
                                                                                <a href="ajax/fancybox/example.html" data-fancybox-type="ajax" class="sm_quickview_handler-list fancybox fancybox.ajax">Quick View </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @foreach($frontCategories as $category)
                            <div class="vc_row vc_row-fluid margin-bottom-60">
                                <div class="vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div id="slider_sw_woo_slider_widget_1" class="responsive-slider woo-slider-default sw-child-cat" data-lg="3" data-md="3" data-sm="2" data-xs="2" data-mobile="1" data-speed="1000" data-scroll="1" data-interval="5000" data-autoplay="false">
                                                <div class="child-top" data-color="#ff9901">
                                                    <div class="box-title pull-left">
                                                        <h3>{{ $category->category_name }}</h3>

                                                        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#child_sw_woo_slider_widget_1" aria-expanded="false">
                                                            <span class="icon-bar"></span>
                                                            <span class="icon-bar"></span>
                                                            <span class="icon-bar"></span>
                                                        </button>
                                                    </div>

                                                    <div class="box-title-right">
                                                        <div class="childcat-content pull-left" id="child_sw_woo_slider_widget_1">
                                                            <ul>
                                                                @php $subCategories = \App\Models\Category::where('parent_id', $category->id)->get(); @endphp
                                                                @foreach($subCategories as $cat)
                                                                <li><a href="{{ route('categoryPage', $cat->slug) }}">{{ $cat->category_name }}</a></li>
                                                                @endforeach

                                                            </ul>
                                                        </div>

                                                        <div class="view-all">
                                                            <a href="{{ route('categoryPage', $category->slug) }}">See All<i class="fa  fa-caret-right"></i></a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="content-slider">
                                                    <div class="childcat-slider-content">
                                                        <!-- Brand -->
                                                        <div class="child-cat-brand pull-left">
                                                            <div class="item-brand">
                                                                <a href="#">
                                                                    <img width="170" height="87" src="public/frontend/images/1903/Brand_1.jpg" class="attachment-170x90 size-170x90" alt="" />
                                                                </a>
                                                            </div>

                                                            <div class="item-brand">
                                                                <a href="#">
                                                                    <img width="170" height="90" src="public/frontend/images/1903/br5.jpg" class="attachment-170x90 size-170x90" alt="" />
                                                                </a>
                                                            </div>

                                                            <div class="item-brand">
                                                                <a href="#">
                                                                    <img width="170" height="90" src="public/frontend/images/1903/br2.jpg" class="attachment-170x90 size-170x90" alt="" />
                                                                </a>
                                                            </div>

                                                            <div class="item-brand">
                                                                <a href="#">
                                                                    <img width="170" height="90" src="public/frontend/images/1903/br3.jpg" class="attachment-170x90 size-170x90" alt="" />
                                                                </a>
                                                            </div>
                                                        </div>

                                                        <!-- slider content -->
                                                        <div class="resp-slider-container">
                                                            <div class="slider responsive">
                                                                @php $cat_products = \App\Models\Product::where('main_category_id', $category->id)->latest()->take(6)->get(); @endphp
                                                                @foreach($cat_products->chunk(2) as $chunk)
                                                                <div class="item product">
                                                                    @foreach($chunk as $cat_product)
                                                                    <div class="item-wrap">
                                                                        <div class="item-detail">
                                                                            <div class="item-content">
                                                                                <!-- rating  -->
                                                                                <div class="reviews-content">
                                                                                    <div class="star"></div>
                                                                                    <div class="item-number-rating">0 Review(s)</div>
                                                                                </div>
                                                                                <!-- end rating  -->

                                                                                <h4>
                                                                                    <a href="{{ route('productDetail', $cat_product->slug) }}" title="voluptate ipsum">{{ $cat_product->product_name }}</a>
                                                                                </h4>

                                                                                <!-- Price -->
                                                                                <div class="item-price">
                                                                                    @if(!empty($cat_product->sale_price))
                                                                                        <del>
                                                                                            Rs. {{ $cat_product->price }}
                                                                                        </del>
                                                                                        <ins>
                                                                                            Rs. {{ $cat_product->sale_price }}

                                                                                        </ins>
                                                                                    @else
                                                                                        <ins>
                                                                                            Rs. {{ $cat_product->price }}
                                                                                        </ins>
                                                                                        @endif
                                                                                    </span>
                                                                                </div>
                                                                            </div>

                                                                            <div class="item-img products-thumb">
                                                                                <a href="simple_product.html">
                                                                                    <div class="product-thumb-hover">
                                                                                        <img 	width="300" height="300" src="{{ asset('public/uploads/product/'.$cat_product->image) }}" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt=""
                                                                                                srcset="{{ asset('public/uploads/product/'.$cat_product->image) }} 300w,{{ asset('public/uploads/product/'.$cat_product->image) }} 150w, {{ asset('public/uploads/product/'.$cat_product->image) }} 180w, {{ asset('public/uploads/product/'.$cat_product->image) }} 600w"
                                                                                                sizes="(max-width: 300px) 100vw, 300px" />
                                                                                    </div>
                                                                                </a>

                                                                                <!-- add to cart, wishlist, compare -->
                                                                                <div class="item-bottom">
                                                                                    <a rel="nofollow" href="#" class="button product_type_simple add_to_cart_button ajax_add_to_cart" title="Add to Cart">Add to cart</a>

                                                                                    <a href="javascript:void(0)" class="compare button" rel="nofollow" title="Add to Compare">Compare</a>

                                                                                    <div class="yith-wcwl-add-to-wishlist ">
                                                                                        <div class="yith-wcwl-add-button show" style="display:block">
                                                                                            <a href="wishlist.html" rel="nofollow" class="add_to_wishlist">Add to Wishlist</a>
                                                                                            <img src="public/frontend/images/wpspin_light.gif" class="ajax-loading" alt="loading" width="16" height="16" style="visibility:hidden" />
                                                                                        </div>

                                                                                        <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
                                                                                            <span class="feedback">Product added!</span>
                                                                                            <a href="#" rel="nofollow">Browse Wishlist</a>
                                                                                        </div>

                                                                                        <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none">
                                                                                            <span class="feedback">The product is already in the wishlist!</span>
                                                                                            <a href="#" rel="nofollow">Browse Wishlist</a>
                                                                                        </div>

                                                                                        <div style="clear:both"></div>
                                                                                        <div class="yith-wcwl-wishlistaddresponse"></div>
                                                                                    </div>

                                                                                    <div class="clear"></div>
                                                                                    <a href="ajax/fancybox/example.html" data-fancybox-type="ajax" class="sm_quickview_handler-list fancybox fancybox.ajax">Quick View </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    @endforeach
                                                                </div>
                                                                @endforeach


                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="best-seller-product">
                                                        <div class="box-slider-title">
                                                            <h2 class="page-title-slider">Best sellers</h2>
                                                        </div>

                                                        <div class="wrap-content">
                                                            <div class="item">
                                                                <div class="item-inner">
                                                                    <div class="item-img">
                                                                        <a href="simple_product.html" title="Sony BRAVIA 4K">
                                                                            <img 	width="180" height="180" src="public/frontend/images/1903/6-180x180.jpg" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt=""
                                                                                    srcset="public/frontend/images/1903/6-180x180.jpg 180w, public/frontend/images/1903/6-150x150.jpg 150w, public/frontend/images/1903/6-300x300.jpg 300w, public/frontend/images/1903/6.jpg 600w"
                                                                                    sizes="(max-width: 180px) 100vw, 180px" />
                                                                        </a>
                                                                    </div>

                                                                    <div class="item-sl pull-left">1</div>

                                                                    <div class="item-content">
                                                                        <!-- rating  -->
                                                                        <div class="reviews-content">
                                                                            <div class="star"></div>
                                                                            <div class="item-number-rating">0 Review(s)</div>
                                                                        </div>
                                                                        <!-- end rating  -->

                                                                        <h4>
                                                                            <a href="simple_product.html" title="Sony BRAVIA 4K">Sony BRAVIA 4K</a>
                                                                        </h4>

                                                                        <div class="item-price">
                                                                            $600.00
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="item">
                                                                <div class="item-inner">
                                                                    <div class="item-img">
                                                                        <a href="simple_product.html" title="iPad Mini 2 Retina">
                                                                            <img 	width="180" height="180" src="public/frontend/images/1903/39-180x180.jpg" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt=""
                                                                                    srcset="public/frontend/images/1903/39-180x180.jpg 180w, public/frontend/images/1903/39-150x150.jpg 150w, public/frontend/images/1903/39-300x300.jpg 300w, public/frontend/images/1903/39.jpg 600w"
                                                                                    sizes="(max-width: 180px) 100vw, 180px" />
                                                                        </a>
                                                                    </div>

                                                                    <div class="item-sl pull-left">2</div>

                                                                    <div class="item-content">
                                                                        <!-- rating  -->
                                                                        <div class="reviews-content">
                                                                            <div class="star"></div>
                                                                            <div class="item-number-rating">0 Review(s)</div>
                                                                        </div>
                                                                        <!-- end rating  -->

                                                                        <h4>
                                                                            <a href="simple_product.html" title="iPad Mini 2 Retina">iPad Mini 2 Retina</a>
                                                                        </h4>

                                                                        <div class="item-price">
                                                                            <del>
                                                                                $300.00
                                                                            </del>

                                                                            <ins>
                                                                                $290.00
                                                                            </ins>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="item">
                                                                <div class="item-inner">
                                                                    <div class="item-img">
                                                                        <a href="simple_product.html" title="ipsum fugiat">
                                                                            <img 	width="180" height="180" src="public/frontend/images/1903/66-180x180.jpg" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt=""
                                                                                    srcset="public/frontend/images/1903/66-180x180.jpg 180w, public/frontend/images/1903/66-150x150.jpg 150w, public/frontend/images/1903/66-300x300.jpg 300w, public/frontend/images/1903/66.jpg 600w"
                                                                                    sizes="(max-width: 180px) 100vw, 180px" />
                                                                        </a>
                                                                    </div>

                                                                    <div class="item-sl pull-left">3</div>

                                                                    <div class="item-content">
                                                                        <!-- rating  -->
                                                                        <div class="reviews-content">
                                                                            <div class="star"></div>
                                                                            <div class="item-number-rating">0 Review(s)</div>
                                                                        </div>
                                                                        <!-- end rating  -->

                                                                        <h4>
                                                                            <a href="simple_product.html" title="ipsum fugiat">ipsum fugiat</a>
                                                                        </h4>

                                                                        <div class="item-price">
                                                                            $250.00
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="item">
                                                                <div class="item-inner">
                                                                    <div class="item-img">
                                                                        <a href="simple_product.html" title="veniam dolore">
                                                                            <img 	width="180" height="180" src="public/frontend/images/1903/50-180x180.jpg" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt=""
                                                                                    srcset="public/frontend/images/1903/50-180x180.jpg 180w, public/frontend/images/1903/50-150x150.jpg 150w, public/frontend/images/1903/50-300x300.jpg 300w, public/frontend/images/1903/50.jpg 600w"
                                                                                    sizes="(max-width: 180px) 100vw, 180px" />
                                                                        </a>
                                                                    </div>

                                                                    <div class="item-sl pull-left">4</div>

                                                                    <div class="item-content">
                                                                        <!-- rating  -->
                                                                        <div class="reviews-content">
                                                                            <div class="star"></div>
                                                                            <div class="item-number-rating">0 Review(s)</div>
                                                                        </div>
                                                                        <!-- end rating  -->

                                                                        <h4>
                                                                            <a href="simple_product.html" title="veniam dolore">Veniam dolore</a>
                                                                        </h4>

                                                                        <div class="item-price">
                                                                            $450.00
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="item">
                                                                <div class="item-inner">
                                                                    <div class="item-img">
                                                                        <a href="simple_product.html" title="voluptate ipsum">
                                                                            <img 	width="180" height="180" src="public/frontend/images/1903/52-180x180.jpg" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt=""
                                                                                    srcset="public/frontend/images/1903/52-180x180.jpg 180w, public/frontend/images/1903/52-150x150.jpg 150w, public/frontend/images/1903/52-300x300.jpg 300w, public/frontend/images/1903/52.jpg 600w"
                                                                                    sizes="(max-width: 180px) 100vw, 180px" />
                                                                        </a>
                                                                    </div>

                                                                    <div class="item-sl pull-left">5</div>

                                                                    <div class="item-content">
                                                                        <!-- rating  -->
                                                                        <div class="reviews-content">
                                                                            <div class="star"></div>
                                                                            <div class="item-number-rating">0 Review(s)</div>
                                                                        </div>
                                                                        <!-- end rating  -->

                                                                        <h4>
                                                                            <a href="simple_product.html" title="voluptate ipsum">voluptate ipsum</a>
                                                                        </h4>

                                                                        <div class="item-price">
                                                                            $550.00
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="vc_row vc_row-fluid margin-bottom-60">
                                <div class="vc_column_container vc_col-sm-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_single_image wpb_content_element vc_align_center">
                                                <figure class="wpb_wrapper vc_figure">
                                                    <a href="#" target="_self" class="vc_single_image-wrapper vc_box_border_grey">
                                                        @if(!empty($category->ad_1))
                                                            <img class="vc_single_image-img" src="{{ asset('public/uploads/category/'.$category->ad_1) }}" width="570" height="220" alt="banner6" title="banner6" />
                                                        @else
                                                            <img class="vc_single_image-img" src="{{ asset('public/uploads/default/ad_default.jpg') }}" width="570" height="220" alt="banner6" title="banner6" />
                                                        @endif
                                                    </a>
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="vc_column_container vc_col-sm-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_single_image wpb_content_element vc_align_center banner-none">
                                                <figure class="wpb_wrapper vc_figure">
                                                    <a href="#" target="_self" class="vc_single_image-wrapper vc_box_border_grey">

                                                        @if(!empty($category->ad_2))
                                                            <img class="vc_single_image-img" src="{{ asset('public/uploads/category/'.$category->ad_2) }}" width="570" height="220" alt="banner6" title="banner6" />
                                                        @else
                                                            <img class="vc_single_image-img" src="{{ asset('public/uploads/default/ad_default.jpg') }}" width="570" height="220" alt="banner6" title="banner6" />
                                                        @endif
                                                    </a>
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                         @endforeach



                            <div class="vc_row vc_row-fluid">
                                <div class="vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
