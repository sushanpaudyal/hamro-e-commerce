@extends('admin.includes.admin_design')

@section('title') Edit Coupon -  {{ config('app.name', 'Laravel') }} @endsection


@section('content')
    <!-- Page Wrapper -->
    <div class="page-wrapper">
        <div class="content container-fluid">

            <!-- Page Header -->
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <h3 class="page-title">Edit Coupon</h3>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('adminDashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Edit Coupon</li>
                        </ul>
                    </div>
                    <div class="col-auto float-right ml-auto">
                        <a href="{{ route('coupon.index') }}" class="btn add-btn"><i class="fa fa-eye"></i> View All Coupons</a>
                    </div>
                </div>
            </div>
            <!-- /Page Header -->

            @include('admin.includes._message')

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{ route('coupon.update', $coupon->id) }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="row">

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="category_name">Coupon Type</label>
                                                <select name="amount_type" id="amount_type" class="form-control">
                                                    <option value="Percentage" @if($coupon->amount_type == "Percantage") selected @endif>Percentage</option>
                                                    <option value="Fixed" @if($coupon->amount_type == "Fixed") selected @endif>Fixed</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="category_name">Coupon Code</label>
                                                <input type="text" class="form-control" name="coupon_code" id="coupon_code" value="{{ $coupon->coupon_code  }}">
                                            </div>
                                        </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="category_name">Coupon Amount</label>
                                            <input type="text" class="form-control" name="amount" id="amount" value="{{ $coupon->amount }}">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="category_name">Expiry Date</label>
                                            <input type="text" class="form-control" name="expiry_date" id="expiry_date" value="{{ $coupon->expiry_date }}">
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="invalidCheck" name="status" @if($coupon->status == 1) checked @endif>
                                            <label class="form-check-label" for="invalidCheck">
                                                Active
                                            </label>
                                        </div>
                                    </div>

                                    <br>
                                    <br>

                                    <div class="text-right float-left">
                                        <button type="submit" class="btn btn-primary">Update Coupon</button>
                                    </div>

                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
    <!-- /Page Wrapper -->

@endsection



@section('js')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>

    <script>
        $(function(){
            $( "#expiry_date" ).datepicker({
                minDate: 0,
                dateFormat: 'yy-mm-dd'
            });
        });
    </script>
    @endsection
