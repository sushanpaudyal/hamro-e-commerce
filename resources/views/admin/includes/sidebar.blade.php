<!-- Sidebar -->
<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
        <div id="sidebar-menu" class="sidebar-menu">
            <ul>
                @if(Session::get('admin_page') == 'dashboard')
                      @php $active = "active" @endphp
                    @else
                    @php $active = "" @endphp
                @endif
                <li class="{{ $active }}">
                    <a href="{{ route('adminDashboard') }}"><i class="la la-dashboard"></i> <span> Dashboard</span> </a>
                </li>
                    @if(Session::get('admin_page') == 'category')
                        @php $active = "active" @endphp
                    @else
                        @php $active = "" @endphp
                    @endif
                <li class="{{ $active }}">
                    <a href="{{ route('category.index') }}"><i class="la la-list-alt"></i> <span> Category</span> </a>
                </li>


                    @if(Session::get('admin_page') == 'product')
                        @php $active = "active" @endphp
                    @else
                        @php $active = "" @endphp
                    @endif
                    <li class="{{ $active }}">
                        <a href="{{ route('product.index') }}"><i class="la la-shopping-cart"></i> <span> Products</span> </a>
                    </li>

                    @if(Session::get('admin_page') == 'web')
                        @php $active = "active" @endphp
                    @else
                        @php $active = "" @endphp
                    @endif
                    <li class="{{ $active }}">
                        <a href="{{ route('coupon.index') }}"><i class="la la-ticket"></i> <span> Coupons</span> </a>
                    </li>


                    @if(Session::get('admin_page') == 'testimonial')
                        @php $active = "active" @endphp
                    @else
                        @php $active = "" @endphp
                    @endif
                    <li class="{{ $active }}">
                        <a href="{{ route('testimonial.index') }}"><i class="la la-comment"></i> <span> Testimonials</span> </a>
                    </li>


                    @if(Session::get('admin_page') == 'theme')
                        @php $active = "active" @endphp
                    @else
                        @php $active = "" @endphp
                    @endif
                <li class="{{ $active }}">
                    <a href="{{ route('theme') }}"><i class="la la-cogs"></i> <span> Settings</span> </a>
                </li>

            </ul>
        </div>
    </div>
</div>
<!-- /Sidebar -->
