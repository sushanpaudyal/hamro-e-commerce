<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\Theme;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       Admin::insert([
           'name' => "Sushan Paudyal",
           'email' => "sushan.paudyal@gmail.com",
           'address' => "Shantinagar Gate",
           'password' => bcrypt("password"),
           'phone' => "9843276470",
           'role_id' => 1,
           'status' => 1,
           'created_at' => now(),
           'updated_at' => now(),
       ]);

        Theme::insert([
            'site_title' => "HamroShop E-Commerce",
            'site_subtitle' => "The Best E-Commerce in Town",
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        User::insert([
            'name' => "Sushan Paudyal",
            'email' => "front@gmail.com",
            'password' => bcrypt("password"),
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
