<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Index Page
Route::get('/', 'IndexController@index')->name('index');

// Product Details Page
Route::get('/product/{slug}', 'FrontEndController@productDetail')->name('productDetail');

// Category
Route::get('/products/{slug}', 'FrontEndController@categoryPage')->name('categoryPage');

// Featured Products
Route::get('/featured_products', 'FrontEndController@featuredProducts')->name('featuredProducts');

// Get product price
Route::post('/product/get-product-price', 'FrontEndController@getProductPrice')->name('getProductPrice');

// Add to Cart
Route::match(['get', 'post'], '/add-cart', 'FrontProductController@addToCart')->name('addToCart');

// Cart
Route::match(['get', 'post'], '/cart', 'FrontProductController@cart')->name('cart');

// Delete Cart Items
Route::get('/cart/delete-product/{id}', 'FrontProductController@deleteCart')->name('deleteCart');

// Update Quantity in Cart
Route::get('/cart/update-quantity/{id}/{quantity}', 'FrontProductController@updateCartQuantity')->name('updateCartQuantity');

// Coupons
Route::post('/cart/apply-coupon', 'FrontProductController@applyCoupon')->name('applyCoupon');


// User Login
Route::get('/user/login', 'FrontUserController@userLogin')->name('userLogin');
Route::post('/user/login', 'FrontUserController@loginUser')->name('loginUser');
Route::get('/user/front/logout', 'FrontUserController@frontUserLogout')->name('frontUserLogout');
Route::post('/user/register', 'FrontUserController@userRegister')->name('userRegister');

Route::get('/confirm/{email}', 'FrontUserController@confirmAccount')->name('confirmAccount');

Route::prefix('/admin')->group(function (){
    // Admin Login
    Route::match(['get', 'post'], '/login', 'AdminLoginController@adminLogin')->name('adminLogin');

        Route::group(['middleware' => ['admin']], function (){
            // Admin Dashboard
            Route::get('/dashboard', 'AdminLoginController@dashboard')->name('adminDashboard');
            // Admin ProfileAd
            Route::get('/profile', 'AdminProfileController@profile')->name('profile');
            // Admin Update
            Route::post('/profile/update/{id}', 'AdminProfileController@updateProfile')->name('updateProfile');
            // Change password
            Route::get('/profile/change_password', 'AdminProfileController@changePassword')->name('changePassword');
            // Check Current Password
            Route::post('/profile/check-password', 'AdminProfileController@chkUserPassword')->name('chkUserPassword');
            // Update Admin Password
            Route::post('/profile/update_password/{id}', 'AdminProfileController@updatePassword')->name('updatePassword');


            // Category Routes
            Route::get('/category', 'CategoryController@category')->name('category.index');
            Route::get('/category/add', 'CategoryController@addCategory')->name('addCategory');
            Route::post('/category/add', 'CategoryController@storeCategory')->name('storeCategory');
            Route::get('/category/edit/{id}', 'CategoryController@editCategory')->name('editCategory');
            Route::post('/category/edit/{id}', 'CategoryController@updateCategory')->name('updateCategory');
            Route::get('/delete-category/{id}', 'CategoryController@deleteCategory')->name('deleteCategory');
            Route::post('/update-category-status', 'CategoryController@updateCategoryStatus')->name('updateCategoryStatus');

            // Theme Settings
            Route::get('/theme', 'ThemeController@theme')->name('theme');
            Route::post('/theme/{id}', 'ThemeController@themeUpdate')->name('themeUpdate');

            // Product
            Route::get('/product', 'ProductsController@product')->name('product.index');
            Route::get('/product/add', 'ProductsController@addProduct')->name('addProduct');
            Route::post('/product/add', 'ProductsController@storeProduct')->name('storeProduct');
            Route::get('/product/edit/{id}', 'ProductsController@editProduct')->name('editProduct');
            Route::post('/product/update/{id}', 'ProductsController@updateProduct')->name('updateProduct');
            Route::get('/delete-product/{id}', 'ProductsController@deleteProduct')->name('deleteProduct');
            Route::post('/update-product-status', 'ProductsController@updateProductStatus')->name('updateProductStatus');
            Route::match(['get', 'post'], '/add-product-attribute/{id}', 'ProductsController@addAttributes')->name('addAttributes');
            Route::get('/delete-attribute/{id}', 'ProductsController@deleteProductAttribute')->name('deleteProductAttribute');
            Route::post('/update-product-attribute/{id}', 'ProductsController@editAttributes')->name('editAttributes');
            Route::match(['get', 'post'], '/add-product-image/{id}', 'ProductsController@addAltImage')->name('addAltImage');
            Route::get('/delete-image/{id}', 'ProductsController@deleteProductImage')->name('deleteProductImage');


            // Testimonials
            Route::get('/testimonial', 'TestimonialController@testimonial')->name('testimonial.index');
            Route::get('/testimonial/add', 'TestimonialController@addTestimonial')->name('testimonial.add');
            Route::post('/testimonial/store', 'TestimonialController@storeTestimonial')->name('testimonial.store');
            Route::get('/testimonial/edit/{id}', 'TestimonialController@editTestimonial')->name('testimonial.edit');
            Route::post('/testimonial/update/{id}', 'TestimonialController@updateTestimonial')->name('testimonial.update');
            Route::get('/delete-testimonial/{id}', 'TestimonialController@delete')->name('testimonial.delete');


            // Coupons
            Route::get('/coupons', 'CouponController@index')->name('coupon.index');
            Route::get('/coupon/add', 'CouponController@add')->name('coupon.add');
            Route::post('/coupon/store', 'CouponController@store')->name('coupon.store');
            Route::get('/coupon/edit/{id}', 'CouponController@edit')->name('coupon.edit');
            Route::post('/coupon/update/{id}', 'CouponController@update')->name('coupon.update');
            Route::get('/delete-coupon/{id}', 'CouponController@delete')->name('coupon.delete');



        });

});

Route::post('/ckeditor', 'CkeditorFileUploadController@store')->name('ckeditor.upload');

Route::get('/admin/logout', 'AdminLoginController@adminLogout')->name('adminLogout');


Route::match(['get', 'post'], '/forget-password', 'AdminLoginController@forgetPassword')->name('forgetPassword');
